/*
 * File         : EalgebraApplication.java
 *
 * Date         : Wednesday 15 April 2020
 *
 * Description  : This is the file for the Spring boot page to be able to run
 *
 * History      : 15/04/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */

package yp.ealgebra.webpage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EalgebraApplication {

	public static void main(String[] args) {
		SpringApplication.run(EalgebraApplication.class, args);
	}

}
