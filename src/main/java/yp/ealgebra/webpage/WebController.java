/*
 * File         :
 *
 * Date         : Wednesday 15 April 2020
 *
 * Description  :
 *
 * History      : 15/04/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */
package yp.ealgebra.webpage;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import yp.ealgebra.questiongen.Questions;

import java.util.Random;

@Controller
public class WebController {
    private Questions questions;


    @GetMapping("/question1")
    public String question1(Model model){
        model.addAttribute("questions", this.questions);
        return "question1";
    }

    @PostMapping("/question1")
    public String question1Submit(@ModelAttribute("questions") Questions questions){
        //model.addAttribute("questions", this.questions);
        this.questions.setAnswer1(questions.getAnswer1());
        return "answer1";
    }

    @GetMapping("/question2")
    public String question2(Model model){
        model.addAttribute("questions",questions);
        return "question2";
    }

    @PostMapping("/question2")
    public String question2Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer2(questions.getAnswer2());
        return "answer2";
    }

    @GetMapping("/question3")
    public String question3(Model model){
        model.addAttribute("questions",questions);
        return "question3";
    }

    @PostMapping("/question3")
    public String question3Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer3(questions.getAnswer3());
        return "answer3";
    }



    @GetMapping("/question4")
    public String question4(Model model){
        model.addAttribute("questions",questions);
        return "question4";
    }

    @PostMapping("/question4")
    public String question4Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer4(questions.getAnswer4());
        return "answer4";
    }


    @GetMapping("/question5")
    public String question5(Model model){
        model.addAttribute("questions",questions);
        return "question5";
    }

    @PostMapping("/question5")
    public String question5Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer5a(questions.getAnswer5a());
        this.questions.setAnswer5b(questions.getAnswer5b());
        return "answer5";
    }


    @GetMapping("/question6")
    public String question6(Model model){
        model.addAttribute("questions",questions);
        return "question6";
    }

    @PostMapping("/question6")
    public String question6Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer6(questions.getAnswer6());
        return "answer6";
    }


    @GetMapping("/question7")
    public String question7(Model model){
        model.addAttribute("questions",questions);
        return "question7";
    }

    @PostMapping("/question7")
    public String question7Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer7(questions.getAnswer7());
        return "answer7";
    }


    @GetMapping("/question8")
    public String question8(Model model){
        model.addAttribute("questions",questions);
        return "question8";
    }

    @PostMapping("/question8")
    public String question8Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer8(questions.getAnswer8());
        return "answer8";
    }


    @GetMapping("/question9")
    public String question9(Model model){
        model.addAttribute("questions",questions);
        return "question9";
    }

    @PostMapping("/question9")
    public String question9Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer9(questions.getAnswer9());
        return "answer9";
    }


    @GetMapping("/question10")
    public String question10(Model model){
        model.addAttribute("questions",questions);
        return "question10";
    }

    @PostMapping("/question10")
    public String question10Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer10(questions.getAnswer10());
        return "answer10";
    }


    @GetMapping("/question11")
    public String question11(Model model){
        model.addAttribute("questions",questions);
        return "question11";
    }

    @PostMapping("/question11")
    public String question11Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer11(questions.getAnswer11());
        return "answer11";
    }


    @GetMapping("/question12")
    public String question12(Model model){
        model.addAttribute("questions",questions);
        return "question12";
    }

    @PostMapping("/question12")
    public String question12Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer12(questions.getAnswer12());
        return "answer12";
    }


    @GetMapping("/question13")
    public String question13(Model model){
        model.addAttribute("questions",questions);
        return "question13";
    }

    @PostMapping("/question13")
    public String question13Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer13(questions.getAnswer13());
        return "answer13";
    }


    @GetMapping("/question14")
    public String question14(Model model){
        model.addAttribute("questions",questions);
        return "question14";
    }

    @PostMapping("/question14")
    public String question14Submit(@ModelAttribute Questions questions){
        this.questions.setAnswer14(questions.getAnswer14());
        return "answer14";
    }

    @RequestMapping("/end")
    public String end(Model model){
        model.addAttribute("questions", this.questions);
        return "end";
    }

    // THE BELOW ARE ONLY FOR TESTING AND DEMONSTRATIVE PURPOSES
    @GetMapping("/answer")
    public String answerForm(Model model){

        model.addAttribute("answer", new Answer());
        return "answer";
    }

    @PostMapping("/answer")
    public String answerSubmit(@ModelAttribute Answer answer){
        return "output";
    }



    @PutMapping
    public String error(Model model){
        return "error";
    }

    @RequestMapping("/")
    public String home(Model model){
        Random random = new Random();
        //long seed = random.nextInt()%1000;      // gives 2000 possibilities
        questions = new Questions();
        return "home";
    }


    @RequestMapping("/instructions")
    public String instructions(){
        return "instructions";
    }

}
