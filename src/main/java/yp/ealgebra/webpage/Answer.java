/*
 * File         : Answer.java
 *
 * Date         : Tuesday 21 April 2020
 *
 * Description  : To store all of the information about an answer given by the user
 *                USED FOR TESTING WITH ANSWER.HTML AND OUTPUT.HTML
 *
 * History      : 21/04/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */
package yp.ealgebra.webpage;

import yp.ealgebra.compiler.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class Answer {
    private String originalString;
    public String latexString;
    private BinaryTree questionTree;
    private BinaryTree answerTree;

    // making some kind of default constructor
    public Answer(){
        this.latexString = "ERROR";
        this.answerTree = new BinaryTree(new BinaryTreeNode("ERROR"));
    }


    public String getOriginalString(){
        return this.originalString;
    }

    public void setOriginalString(String string){
        System.out.println("It did go in here tho");
        this.originalString = string;
        // then you also want to do the stuff here with building the string
        String tempString = this.originalString + "\r";
        InputStream is = new ByteArrayInputStream(tempString.getBytes());
        TreeGenerator answerComp = new TreeGenerator(is);
        answerComp.ReInit(is);
        try{
            this.answerTree = answerComp.Start(System.out);
        }
        catch(Exception e){
            System.out.println("Exception caught in yp.ealgebra.webpage.Answer.java.setOriginalString\n"
            +"String used = " + this.originalString +
                    "\n Exception : " + e);
        }
        // THIS LINE MAY BE REMOVED STILL
        this.answerTree = StandardiseTree.standardise2(this.answerTree);
        this.latexString = this.answerTree.toString();
    }

    public void markQuestion(){
        BinaryTree tree = new BinaryTree();
        BinaryTreeNode root = new BinaryTreeNode("-");
        root.setLeft(this.answerTree);
        root.setLeft(this.questionTree);
        tree.setRoot(root);
        tree = StandardiseTree.standardise2(tree);
        boolean correct;
        correct = tree.getRoot().getValue().toString().equals("0") && !tree.getRoot().hasChildren();
    }

    public void setQuestionString(String question){
        InputStream is = new ByteArrayInputStream(question.getBytes());
        TreeGenerator questComp = new TreeGenerator(is);
        questComp.ReInit(is);
        try{
            this.questionTree = questComp.Start(System.out);
        }
        catch(Exception e) {
            System.out.println("Exception caught in yp.ealgebra.webpage.Answer.java.setQuestionString\n" +
                    "String used = " + question +
                    "\n Exception : " + e);
        }
    }

    public void setLatexString(String inputString){
        this.latexString = inputString;
    }

    public String getLatexString(){
        return this.latexString;
    }
}
