/*
 * File         : Fraction.java
 *
 * Date         : Wednesday 1 January 2020
 *
 * Description  : The fraction data type, to be used in all fraction based questions
 *
 * History      : 17/01/2020   --v1.07
 *
 * Author       : Matthew Dalzell   100227419
 */

package yp.ealgebra.questiongen;


import yp.ealgebra.compiler.BinaryTreeNode;

public class Fraction {
    private int numerator;
    private int denominator;

    public Fraction(int num, int denom){
        this.numerator = num;
        this.denominator = denom;
        this.simplify();
    }

    public Fraction(String string){
        String[] parts = string.split("/");
        if(parts.length == 1){
            this.numerator = Integer.parseInt(parts[0]);
            this.denominator = 1;
        }
        else{
            this.numerator = Integer.parseInt(parts[0]);
            this.denominator = Integer.parseInt(parts[1]);
        }
        this.simplify();
    }

    // this will be the default, the idea is that these will be changed
    public Fraction(){
        this.numerator = 0;
        this.denominator = 1;
    }



    // can deal with it in the format (a/(b/c))
    public Fraction(int num, Fraction denominator){
        Fraction temp = new Fraction(num, 1);
        Fraction temp2 = temp.div(denominator);
        this.numerator = temp2.getNumerator();
        this.denominator = temp2.getDenominator();
        // how can we use a method on this?


    }

    // making the methods for manipulating them
    // numeric methods
    public Fraction add(Fraction fract2){
        // so you wnat to: multiply the denominators to find the new one, then add the values times the opposite
        // denomninator to find the new numerator. Finally break them down using the gcd
        int ansDenom = this.denominator * fract2.getDenominator();
        int ansNum = this.numerator*fract2.getDenominator() + this.denominator * fract2.getNumerator();

        Fraction answer = new Fraction(ansNum, ansDenom);
        answer.simplify();
        return answer;
    }

    public Fraction sub(Fraction fract2){
        int temp = fract2.getNumerator();
        temp = temp * (-1);
        Fraction tempFract = new Fraction(temp, fract2.getDenominator());
        return add(tempFract);
    }

    public Fraction mult(Fraction fract2){
        int newNum = this.numerator * fract2.getNumerator();
        int newDenom = this.denominator * fract2.getDenominator();
        Fraction result = new Fraction(newNum, newDenom);
        result.simplify();
        return result;
    }

    public Fraction div(Fraction fract2){
        Fraction tempFract = new Fraction(fract2.getDenominator(), fract2.getNumerator());
        return mult(tempFract);
    }

    public Fraction add(int input){
        Fraction output = new Fraction((this.numerator+(input*this.denominator)),this.denominator );
        output.simplify();
        return output;
    }

    public Fraction sub(int input){
        Fraction output = new Fraction((this.numerator-(input*this.denominator)),this.denominator );
        output.simplify();
        return output;
    }

    public Fraction mult(int input){
        Fraction temp = new Fraction(input, 1);
        return mult(temp);
    }

    public Fraction div(int input){
        Fraction temp = new Fraction(input, 1);
        return div(temp);
    }

    public Fraction pow(int input){
        Fraction output = new Fraction();
        output.setNumerator((int)Math.pow(this.numerator, input));
        output.setDenominator((int)Math.pow(this.denominator, input));
        return output;
    }


    public boolean equals(Fraction fract2){
        this.simplify();
        fract2.simplify();

        return this.numerator == fract2.getNumerator() && this.denominator == fract2.getDenominator();
    }

    public boolean equals(int number){
        Fraction temp = new Fraction();
        temp.setNumerator(number);
        temp.setDenominator(1);
        return equals(temp);
    }

    public void simplify(){
        // first we need to find the gcd, and then you want to divide both of the current values
        // first, make copies of the values to not damage them
        if(this.denominator != 1) {
            int a = Math.abs(this.denominator);
            int b = Math.abs(this.numerator);
            //System.out.println(a);
            //System.out.println(b);
            while (b != 0) {
                int temp = b;
                b = a % b;
                a = temp;
            }
            // now 'a' should have the GCD in it
            // what will it be when there is not one?
            this.numerator = this.numerator / a;
            this.denominator = this.denominator / a;
            // then we want to check negatives
        }
        if(this.denominator < 0 && this.numerator < 0){
            // then we actually make them both positive
            this.denominator = this.denominator * (-1);
            this.numerator = this.numerator *(-1);
        }
        // if the bottom one is and the top one is not
        if(this.numerator > 0 && this.denominator < 0){
            this.numerator = this.numerator * (-1);
            this.denominator = this.denominator * (-1);
        }
    }

    public BinaryTreeNode toNode(){
        BinaryTreeNode output = new BinaryTreeNode();
        this.simplify();
        if(this.getDenominator() == 1)
            output.setValue(this.numerator + "");
        else if(this.getNumerator() == 0)
            output.setValue("0");
        else{
            output.setValue("/");
            BinaryTreeNode left = new BinaryTreeNode(this.numerator + "");
            BinaryTreeNode right = new BinaryTreeNode(this.denominator + "");
            output.setRight(right);
            output.setLeft(left);
        }
        return output;
    }

    public boolean equalsZero(){
        return this.numerator == 0;
    }

    // getters
    public int getNumerator(){
        return this.numerator;
    }

    public int getDenominator(){
        return this.denominator;
    }


    // setters
    public void setNumerator(int num){
        this.numerator = num;
        //this.simplify();
    }

    public void setDenominator(int denom){
        this.denominator = denom;
    }



    public String toString(){
        if(this.denominator != 1) {
            return this.numerator + "/" + this.denominator;
        }
        String output = "";
        output = output + this.numerator;
        return output;
    }

    public String toLatexString(){
        // so this wants to make it in the form \frac{}{}
        this.simplify();
        if(this.denominator == 1){
            return ""+ this.numerator;
        }
        if(this.numerator % this.denominator == 0){
            return ""+(this.numerator/ this.denominator);
        }

        String outString = "";
        outString = outString + "\\frac{"+this.numerator + "}{"+this.denominator+"}";
        return outString;


    }

}
