/*
 * File         : Main.java
 *
 * Date         : Wednesday 1 January 2020
 *
 * Description  : Running the questions, this is mainly used for testing that all has worked
 *
 * History      : 17/01/2020   --v1.23
 *
 * Author       : Matthew Dalzell   100227419
 */

package yp.ealgebra.questiongen;


import yp.ealgebra.webpage.Answer;

import java.util.Random;
import java.lang.Math;


public class Main {

    public static void main(String[] args) {
        Random random = new Random();
        long seed = random.nextInt();
        Questions questions = new Questions();
        System.out.println("--- QUESTION 1 ---");
        questions.printQuestion1();
        questions.printAnswer1();
        System.out.println(questions.getQuestion1().compare(questions.getAnswer1()));
        System.out.println("--- QUESTION 2 ---");
        questions.printQuestion2();
        questions.printAnswer2();
        System.out.println(questions.getQuestion2().compare(questions.getAnswer2()));
        System.out.println("--- QUESTION 3 ---");
        questions.printQuestion3();
        questions.printAnswer3();
        System.out.println(questions.getQuestion3().compare(questions.getAnswer3()));
        System.out.println("--- QUESTION 4 ---");
        questions.printQuestion4();
        questions.printAnswer4();
        System.out.println(questions.getQuestion4().compare(questions.getAnswer4()));
        System.out.println("--- QUESTION 5 ---");
        System.out.println("SKIPPING QUSTION 5 BECAUSE IT IS A MORE COMPLEX MARKING");
        /*questions.printQuestion5();
        questions.printAnswer5();
        System.out.println(questions.getQuestion5().compare(questions.getAnswer5()));
        */
        System.out.println("--- QUESTION 6 ---");
        questions.printQuestion6();
        questions.printAnswer6();
        System.out.println(questions.getQuestion6().compare(questions.getAnswer6()));
        System.out.println("--- QUESTION 7 ---");
        questions.printQuestion7();
        questions.printAnswer7();
        System.out.println(questions.getQuestion7().compare(questions.getAnswer7()));
        System.out.println("--- QUESTION 8 ---");
        questions.printQuestion8();
        questions.printAnswer8();
        System.out.println(questions.getQuestion8().compare(questions.getAnswer8()));
        System.out.println("--- QUESTION 9 ---");
        questions.printQuestion9();
        questions.printAnswer9();
        System.out.println(questions.getQuestion9().compare(questions.getAnswer9()));
        System.out.println("--- QUESTION 10 ---");
        questions.printQuestion10();
        questions.printAnswer10();
        System.out.println(questions.getQuestion10().compare(questions.getAnswer10()));
        System.out.println("--- QUESTION 11 ---");
        questions.printQuestion11();
        questions.printAnswer11();
        System.out.println(questions.getQuestion11().compare(questions.getAnswer11()));
        System.out.println("--- QUESTION 12 ---");
        questions.printQuestion12();
        questions.printAnswer12();
        System.out.println(questions.getQuestion12().compare(questions.getAnswer12()));
        System.out.println("--- QUESTION 13 ---");
        questions.printQuestion13();
        questions.printAnswer13();
        System.out.println(questions.getQuestion13().compare(questions.getAnswer13()));
        System.out.println("--- QUESTION 14 ---");
        questions.printQuestion14();
        questions.printAnswer14();
        System.out.println(questions.getQuestion14().compare(questions.getAnswer14()));


    }
}
