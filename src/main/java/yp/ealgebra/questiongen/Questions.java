/*
 * File         : GenerateQuestions.java
 *
 * Date         : Thursday 14 May 2020
 *
 * Description  : Making the questions for the website to ask, which takes in the seed as parameter and builds questions
 *
 * History      : 14/05/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */
package yp.ealgebra.questiongen;

import yp.ealgebra.compiler.BinaryTree;
import yp.ealgebra.compiler.BinaryTreeNode;
import yp.ealgebra.compiler.StandardiseTree;
import yp.ealgebra.compiler.TreeGenerator;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Random;

public class Questions {
    private final long seed;
    // the questions
    private final BinaryTree question1;
    private final BinaryTree question2;
    private final BinaryTree question3;
    private final BinaryTree question4;
    private final BinaryTree question5;
    private final BinaryTree[] question5Answers;
    private final BinaryTree question6;
    private final BinaryTree question7;
    private final BinaryTree question8;
    private final BinaryTree question9;
    private final BinaryTree question10;
    private final BinaryTree question11;
    private final BinaryTree question12;
    private final BinaryTree question13;
    private final BinaryTree question14;
    // the answers
    private  BinaryTree answer1;
    private  BinaryTree answer2;
    private  BinaryTree answer3;
    private  BinaryTree answer4;
    private  BinaryTree answer5a;
    private  BinaryTree answer5b;
    private  BinaryTree answer6;
    private  BinaryTree answer7;
    private  BinaryTree answer8;
    private  BinaryTree answer9;
    private  BinaryTree answer10;
    private  BinaryTree answer11;
    private  BinaryTree answer12;
    private  BinaryTree answer13;
    private  BinaryTree answer14;

    private String ans1String;
    private String ans2String= "";
    private String ans3String= "";
    private String ans4String= "";
    private String ans5aString= "";
    private String ans5bString = "";
    private String ans6String= "";
    private String ans7String= "";
    private String ans8String= "";
    private String ans9String= "";
    private String ans10String= "";
    private String ans11String= "";
    private String ans12String= "";
    private String ans13String= "";
    private String ans14String= "";

    public Questions(){
        Random seedGen = new Random();
        this.seed = Math.abs(seedGen.nextLong())%999;
        int a,b,c,d,e,f;
        Fraction x,y;
        // then you want to generate all of the questions you want to make
        Random random = new Random(seed);

        // making of the first question
        // EXPANSION
        a = (random.nextInt() % 20);
        while(a == 0)
            a = (random.nextInt()%20);
        b = a;
        while(a == b || b == 0)
            b = random.nextInt() % 20;

        // then you want to build this into a string
        String quest_string = "(x"+addSign(a)+")(x"+addSign(b)+")";
        // then we want the actual method to store it as a tree
        this.question1 = stringToTree(quest_string);
        //this.answer1 = StandardiseTree.standardise(this.question1);

        // making of the second question
        // FACTORISATION
        a = 0;
        while(a == 0)
            a = random.nextInt() % 20;
        b = a;
        while(a == b || b == 0 )
            b = random.nextInt()%20;
        quest_string = "(x"+addSign(a)+")(x"+addSign(b)+")";
        //this.answer2 = stringToTree(quest_string);
        this.question2 = StandardiseTree.standardise(stringToTree(quest_string));

        // making the third question
        // MORE COMPLEX QUADRATIC
        a = 0;
        while(a == 0 || a == 1)
            a = Math.abs(random.nextInt() % 20);
        b = 0;
        while(b == 0)
            b = random.nextInt() % 20;
        c = 0;
        while(c == 0 || c == 1 || c == a)
            c = Math.abs(random.nextInt() % 20);
        d = 0;
        while(d == 0 || d == b)
            d = random.nextInt() % 20;
        quest_string = "("+a+"x"+addSign(b)+")("+c+"x"+addSign(d)+")";

        //System.out.println(quest_string);
        this.question3 = stringToTree(quest_string);
        //this.answer3 = StandardiseTree.standardise(this.question3);

        // making of the fourth question
        // POWER MULTIPLICATION
        a = (Math.abs(random.nextInt())%18)+2;
        b = (Math.abs(random.nextInt())%18)+2;
        c = (Math.abs(random.nextInt())%18)+2;
        d = (Math.abs(random.nextInt())%18)+2;
        e = (Math.abs(random.nextInt())%18)+2;
        f = (Math.abs(random.nextInt())%18)+2;
        quest_string = "("+a+"x^"+b+")("+c+"x^"+d+")("+e+"x^"+f+")";
        this.question4 = stringToTree(quest_string);
        //this.answer4 = StandardiseTree.standardise(this.question4);

        // making of the fifth question
        a = 0;
        while(a == 0)
            a = random.nextInt()%20;
        b = 0;
        while(b == 0 || b == a)
            b = random.nextInt()%20;
        c = 0;
        while(c == 0 || c == b || c == a)
            c = random.nextInt()%20;
        String top = "(x"+ addSign(a)+")(x"+ addSign(b)+")";
        String bottom ="(x"+ addSign(a)+")(x"+ addSign(c)+")";
        BinaryTree topTree = stringToTree(top);
        BinaryTree bottomTree = stringToTree(bottom);
        // expand them to make it more complicated
        topTree = StandardiseTree.standardise(topTree);
        bottomTree = StandardiseTree.standardise(bottomTree);
        BinaryTreeNode tempRoot = new BinaryTreeNode("/");
        tempRoot.setRight(bottomTree);
        tempRoot.setLeft(topTree);
        this.question5 = new BinaryTree();
        this.question5.setRoot(tempRoot);
        // then you need to make the answers up
        // TODO : need to make the answering work with two parts
        topTree = new BinaryTree();
        bottomTree = new BinaryTree();
        // then you want to make the answers up nicely
        top = "(x"+ addSign(b)+")";
        bottom = "(x"+ addSign(c)+")";
        this.question5Answers = new BinaryTree[2];
        topTree = stringToTree(top);
        bottomTree = stringToTree(bottom);
        this.question5Answers[0] = topTree;
        this.question5Answers[1] = bottomTree;
        tempRoot = new BinaryTreeNode("/");
        tempRoot.setRight(bottomTree);
        tempRoot.setLeft(topTree);
        //this.answer5 = new BinaryTree();
        //this.answer5.setRoot(tempRoot);

        // MAKING OF FRACTION BASED QUESTIONS

        // difficult fraction adding
        // a number between 10 and 100
        x = new Fraction();
        x.setNumerator((Math.abs(random.nextInt())%90) + 10);
        x.setDenominator(x.getNumerator());
        while(x.getNumerator() == x.getDenominator()){
            x.setDenominator((Math.abs(random.nextInt())%90)+10);
        }
        // doing the same agian for y
        y = new Fraction();
        y.setNumerator((Math.abs(random.nextInt())%90) + 10);
        y.setDenominator(y.getNumerator());
        while(y.getNumerator() == y.getDenominator())
            y.setDenominator((Math.abs(random.nextInt())%90) + 10);
        // then you need to form this into a quesiton
        x.simplify();
        y.simplify();
        quest_string = "("+x.toString() + ") + (" + y.toString()+")";
        this.question6 = stringToTree(quest_string);
        //this.answer6 = StandardiseTree.standardise(this.question6);


        // difficult fraction subtraction
        x = new Fraction();
        y = new Fraction();
        x.setNumerator((Math.abs(random.nextInt())%50) + 10);
        x.setDenominator(x.getNumerator());
        while(x.getNumerator() == x.getDenominator())
            x.setDenominator((Math.abs(random.nextInt())%50) + 10);
        y.setNumerator((Math.abs(random.nextInt())%50) + 10);
        y.setDenominator(y.getNumerator());
        while(y.getNumerator() == y.getDenominator())
            y.setDenominator((Math.abs(random.nextInt())%50) + 10);
        x.simplify();
        y.simplify();
        quest_string = "("+x.toString() + ") - (" + y.toString()+")";
        this.question7 = stringToTree(quest_string);
        //this.answer7 = StandardiseTree.standardise(this.question7);

        // making a bit of a trap question
        a = 0;
        while(a <= 5 && a >= -5)
            a = random.nextInt()%15;
        b = 0;
        while(b <= 5 && b >= -5 || a == b)
            b = random.nextInt()%15;
        x = new Fraction();
        x.setNumerator(0);
        while(x.getNumerator() <= 5 && x.getNumerator() >= -5)
            x.setNumerator(random.nextInt()%15);
        x.setDenominator(x.getNumerator());
        while(x.getNumerator() == x.getDenominator())
            x.setDenominator((Math.abs(random.nextInt())%5) + 5);
        x.simplify();
        quest_string = a + "x" + addSign(b) + "x^2" + addSign(x) + "x";
        this.question8 = stringToTree(quest_string);
        //this.answer8 = StandardiseTree.standardise(this.question8);

        // fraction times int
        a = (random.nextInt()%15) + 5;
        x = new Fraction();
        x.setNumerator(Math.abs((random.nextInt())%50) + 10);
        x.setDenominator(x.getNumerator());
        while(x.getDenominator() == x.getNumerator() || x.getDenominator() == a || (x.getDenominator() <= 1 &&
                x.getDenominator() >= -1))
            x.setDenominator(Math.abs((random.nextInt()%15)+5));
        x.simplify();
        quest_string = a + "*("+x.toString()+")";
        this.question9 = stringToTree(quest_string);
        //this.answer9 = StandardiseTree.standardise(this.question9);

        // fraction multiplying
        x = new Fraction();
        y = new Fraction();
        x.setNumerator((Math.abs(random.nextInt())%15)+5);
        x.setDenominator(x.getNumerator());
        while(x.getNumerator() == x.getDenominator() || x.getDenominator() == 1)
            x.setDenominator((Math.abs(random.nextInt())%15)+5);
        y.setNumerator((Math.abs(random.nextInt())%15)+5);
        y.setDenominator(y.getNumerator());
        while(y.getNumerator() == y.getDenominator())
            y.setDenominator((Math.abs(random.nextInt())%15)+5);
        x.simplify();
        y.simplify();
        quest_string = "("+x.toString()+") * ("+y.toString()+")";
        this.question10 = stringToTree(quest_string);
        //this.answer10 = StandardiseTree.standardise(this.question10);

        // fraction multiplying with x
        a = (Math.abs(random.nextInt()) % 8)+1;
        b = a;
        while(b == a){
            b = (Math.abs(random.nextInt()) % 8)+1;
        }
        x = new Fraction();
        y = new Fraction();
        x.setNumerator(0);
        while(x.getNumerator() < 5 && x.getNumerator() > -5)
            x.setNumerator(random.nextInt()%15);
        x.setDenominator(x.getNumerator());
        while(x.getNumerator() == x.getDenominator() || x.getNumerator() < 5)
            x.setNumerator(Math.abs(random.nextInt())%20);

        y.setNumerator(0);
        while(y.getNumerator() < 5 && y.getNumerator() > -5)
            y.setNumerator(random.nextInt()%15);
        y.setDenominator(y.getNumerator());
        while(y.getNumerator() == y.getDenominator() || y.getNumerator() < 5)
            y.setNumerator(Math.abs(random.nextInt())%20);
        x.simplify();
        y.simplify();
        quest_string = "("+x.toString()+"x) * ("+y.toString()+"x)";
        this.question11 = stringToTree(quest_string);
        //this.answer11 = StandardiseTree.standardise(this.question11);

        // making a power question for integers
        a = (Math.abs(random.nextInt()) % 3) + 2;
        // then making the other value
        b = (Math.abs(random.nextInt())%8)+2;
        quest_string = b + "^" + a;
        this.question12 = stringToTree(quest_string);
        this.answer12 = StandardiseTree.standardise(this.question12);

        // making a power question for Fractions
        a = (Math.abs(random.nextInt())%3)+2;
        x = new Fraction();
        x.setNumerator((Math.abs(random.nextInt())%10)+5);
        x.setDenominator(x.getNumerator());
        while(x.getNumerator() == x.getDenominator())
            x.setDenominator((Math.abs(random.nextInt())%10)+5);
        quest_string = "("+x.toString()+")^"+a;
        this.question13 = stringToTree(quest_string);
        //this.answer13 = StandardiseTree.standardise(this.question13);

        // finally making question14 as something a bit more complex but following suit to the ones from before
        a = Math.abs(random.nextInt())%2 + 2;
        x = new Fraction();
        y = new Fraction();
        x.setNumerator((Math.abs(random.nextInt())%5)+5);
        x.setDenominator(x.getNumerator());
        while(x.getDenominator()>=x.getNumerator())
            x.setDenominator((Math.abs(random.nextInt())%x.getNumerator())+1);
        y.setNumerator(x.getNumerator());
        while(y.getNumerator() == x.getNumerator())
            y.setNumerator((Math.abs(random.nextInt())%5)+5);
        y.setDenominator(y.getNumerator());
        while(y.getDenominator() == y.getNumerator() || y.getDenominator() == x.getDenominator())
            y.setDenominator((Math.abs(random.nextInt())%y.getNumerator()) + 1);
        quest_string = "(("+x.toString()+")+("+y.toString()+"))^"+a;
        this.question14 = stringToTree(quest_string);
        //this.answer14 = StandardiseTree.standardise(this.question14);

    }


    // compiling the made strings into trees
    private BinaryTree stringToTree(String inputString){
        inputString = inputString + "\r";
        BinaryTree output = new BinaryTree();
        output.setRoot(new BinaryTreeNode("Syntax Error"));
        InputStream is = new ByteArrayInputStream(inputString.getBytes());
        TreeGenerator compiler = new TreeGenerator(is);
        try{
            output = compiler.Start(System.out);
        }
        catch(Exception e){
            System.out.println("Exception caught in : package yp.ealgebra.questiongen"  +
                             "\nException           : " + e +
                             "\nString failed on    :" + inputString);
        }
        return output;
    }

    private String addSign(int a){
        String output;
        if(a < 0){
            a = a *(-1);
            output = "-("+a+")";
            return output;
        }
        output = "+("+a+")";
        return output;
    }

    private String addSign(Fraction a){
        String output;
        int numerator = a.getNumerator();
        if(numerator < 0){
            numerator = numerator * -1;
            output = "-"+numerator+a.getDenominator();
            return output;
        }
        output = "+"+a.toString();
        return output;

    }

    public String score(){
        String output = "";
        // so then we have to make all of the questions;
        boolean[] correct = new boolean[14];
        Arrays.fill(correct, false);
        // this should only be done if all of them have been answered
        // checks the format and then checks the answer was still correct
        // FIXME : You can check for brackets here to ensure it is done properly
        try {
            if (this.answer1.getRoot().getValue().toString().equals("*") || !this.question1.compare(this.answer1)) {
                correct[0] = false;
            } else {
                correct[0] = true;
            }
        }
        catch(Exception e){
            correct[0] = false;
        }

        // then for question 2
        try {
            if (this.answer2.getRoot().getValue().toString().equals("*") && this.question2.compare(this.answer2))
                correct[1] = true;
            else
                correct[1] = false;
        }
        catch(Exception e){
            correct[1] = false;
        }


        // for question 3
        try {
            if (this.answer3.getRoot().getValue().toString().equals("*") || !this.question3.compare(this.answer3))
                correct[2] = false;
            else
                correct[2] = true;
        }
        catch(Exception e){
            correct[2] = false;
        }

        // question 4
        try {
            if (this.answer4.getRoot().getRight().getValue().toString().equals("^") &&
                    this.answer4.getRoot().getValue().toString().equals("*") &&
                    StandardiseTree.isNum(this.answer4.getRoot().getLeft()))
                correct[3] = true;
            else
                correct[3] = false;
        }
        catch(Exception e){
            correct[3] = false;
        }

        // question 5
        try {
            if (this.answer5a.compare(this.question5Answers[0]) && this.answer5b.compare(this.question5Answers[1]))
                correct[4] = true;
            else
                correct[4] = false;
        }
        catch(Exception e){
            correct[4] = false;
        }

        // quesiton 6
        BinaryTree q6real = StandardiseTree.standardise(this.question6);
        try{
            // check if it is a fraction first
            if(q6real.getRoot().getValue().toString().equals("/")) {
                // then this should be a fraction, check they are working with the same denominator
                if (Integer.parseInt(q6real.getRoot().getRight().getValue().toString()) ==
                        Integer.parseInt(this.answer6.getRoot().getRight().getValue().toString()) &&
                        q6real.compare(this.answer6)) {
                    correct[5] = true;
                } else {
                    correct[5] = false;
                }
            }
            else{
                // then they should both be integers
                if(q6real.compare(this.answer6))
                    correct[5] = true;
                else
                    correct[5] = false;
            }
        }
        catch(Exception e){
            correct[5] = false;
        }

        // question 7
        BinaryTree q7real = StandardiseTree.standardise(this.question7);
        // then we want to compare them and ensure their format
        try{
            BinaryTreeNode realRoot = q7real.getRoot();
            BinaryTreeNode theirRoot = this.answer7.getRoot();
            boolean keepGoing = true;
            // they may have just put the question back in
            if(realRoot.getValue().toString().equals("-")){
                if(realRoot.hasLeft()){
                    correct[6] = false;
                    keepGoing = false;
                }
                else{
                    realRoot = realRoot.getRight();
                    theirRoot = theirRoot.getRight();
                }
                // then everything we worry about has to be on the right, well we can extract this root
            }
            // then checking their denominators
            if(keepGoing){
                // then we want to do a similar comparison to before
                if(realRoot.getValue().toString().equals("/")){
                    if(Integer.parseInt(realRoot.getRight().getValue().toString()) ==
                            Integer.parseInt(theirRoot.getRight().toString()) && q7real.compare(this.answer7))
                        correct[6] = true;
                    else
                        correct[6] = false;
                }
                else{
                    if(q7real.compare(this.answer7))
                        correct[6] = true;
                    else
                        correct[6] = false;
                }
            }
        }
        catch(Exception e){
            correct[6] = false;
        }

        // question 8
        try {
            if (StandardiseTree.treeDepth(this.answer8) >= StandardiseTree.treeDepth(this.question8))
                correct[7] = false;
            else {
                if (this.answer8.compare(this.question8))
                    correct[7] = true;
                else
                    correct[7] = false;
            }
        }
        catch(Exception e){
            correct[7] = false;
        }

        // question 9
        try {
            if (this.answer9.getRoot().getValue().toString().equals("*")) {
                correct[8] = false;
            } else {
                try {
                    if (this.answer9.compare(this.question9))
                        correct[8] = true;
                    else
                        correct[8] = false;
                } catch (Exception e) {
                    correct[8] = false;
                }
            }
        }
        catch(Exception e){
            correct[8] = false;
        }

        // question 10
        try {
            if (this.answer10.getRoot().getValue().toString().equals("*")) {
                // then we throw it out
                correct[9] = false;
            } else {
                if (this.answer10.compare(this.question10))
                    correct[9] = true;
                else
                    correct[9] = false;
            }
        }
        catch(Exception e){
            correct[9] = false;
        }

        // question 11
        try {
            BinaryTree q11real = StandardiseTree.standardise(this.question11);
            if (this.answer11.getRoot().getValue().toString().equals("*") &&
                    this.answer11.getRoot().getRight().getValue().toString().equals("^") && this.answer11.compare(q11real)) {
                correct[10] = true;
            } else {
                correct[10] = false;
            }
        }
        catch(Exception e){
            correct[10] = false;
        }

        // question 12
        try{
            if(this.answer12.compare(this.question12) &&
                    StandardiseTree.isInt(this.answer12.getRoot().getValue().toString()))
                correct[11] = true;
            correct[11] = false;
        }
        catch(Exception e){
            correct[11] = false;
        }

        // question 13
        try {
            if (StandardiseTree.isNum(this.answer13.getRoot()) && this.answer13.compare(this.question13))
                correct[12] = true;
            else
                correct[12] = false;
        }
        catch(Exception e){
            correct[12] = false;
        }

        // question 14
        try{
            if(StandardiseTree.isNum(this.answer14.getRoot()) && this.question14.compare(this.answer14)){
                correct[13] = true;
            }
            else{
                correct[13] = false;
            }
        }
        catch(Exception e){
            correct[13] = false;
        }



        int score = 0;
        int numCorrect = 0;
        for(int i = 0; i < 14; i++) {
            //System.out.println(correct[i]);
            if(correct[i]) {
                score += Math.pow(2, i);
                numCorrect++;
            }
        }
        StringBuilder mistakes = new StringBuilder();
        if(numCorrect < 14) {
            mistakes = new StringBuilder("You made mistakes on the following question(s): ");
            boolean firstMistake = true;
            for(int i =0; i < 14; i++){
                if(!correct[i]){
                    if(firstMistake){
                        mistakes.append(i + 1);
                        firstMistake = false;
                    }
                    else{
                        mistakes.append(", ").append(i + 1);
                    }

                }
            }
        }
        output = "You scored: " + numCorrect + "/14. " + System.lineSeparator() + "Your code is : "+this.seed + " - " + score
                + ". " + mistakes;
        return output;
    }

    // accessor methods, used for testing and maybe too in the final thing

    // printers
    // questions
    public void printQuestion1(){
        System.out.println(this.question1.toString());
    }

    public void printQuestion2(){
        System.out.println(this.question2.toString());
    }

    public void printQuestion3(){
        System.out.println(this.question3.toString());
    }

    public void printQuestion4(){
        System.out.println(this.question4.toString());
    }

    public void printQuestion5(){
        System.out.println(this.question5.toString());
    }

    public void printQuestion6(){
        System.out.println(this.question6.toString());
    }

    public void printQuestion7(){
        System.out.println(this.question7.toString());
    }

    public void printQuestion8(){
        System.out.println(this.question8.toString());
    }

    public void printQuestion9(){
        System.out.println(this.question9.toString());
    }

    public void printQuestion10(){
        System.out.println(this.question10.toString());
    }

    public void printQuestion11(){
        System.out.println(this.question11.toString());
    }

    public void printQuestion12(){
        System.out.println(this.question12.toString());
    }

    public void printQuestion13(){
        System.out.println(this.question13.toString());
    }

    public void printQuestion14(){
        System.out.println(this.question14.toString());
    }

    // answers
    // TODO : Remove these, they are only for testing purposes
    public void printAnswer1(){
        System.out.println(this.answer1.toString());
    }

    public void printAnswer2(){
        System.out.println(this.answer2.toString());
    }

    public void printAnswer3(){
        System.out.println(this.answer3.toString());
    }

    public void printAnswer4(){
        System.out.println(this.answer4.toString());
    }

    public void printAnswer5a(){
        System.out.println(this.answer5a.toString());
    }

    public void printAnswer5b(){
        System.out.println(this.answer5b.toString());
    }

    public void printAnswer6(){
        System.out.println(this.answer6.toString());
    }

    public void printAnswer7(){
        System.out.println(this.answer7.toString());
    }

    public void printAnswer8(){
        System.out.println(this.answer8.toString());
    }

    public void printAnswer9(){
        System.out.println(this.answer9.toString());
    }

    public void printAnswer10(){
        System.out.println(this.answer10.toString());
    }

    public void printAnswer11(){
        System.out.println(this.answer11.toString());
    }

    public void printAnswer12(){
        System.out.println(this.answer12.toString());
    }

    public void printAnswer13(){
        System.out.println(this.answer13.toString());
    }

    public void printAnswer14(){
        System.out.println(this.answer14.toString());
    }

    // getters
    public BinaryTree getQuestion1(){
        return this.question1;
    }

    public BinaryTree getQuestion2(){
        return this.question2;
    }

    public BinaryTree getQuestion3(){
        return this.question3;
    }

    public BinaryTree getQuestion4(){
        return this.question4;
    }

    public BinaryTree getQuestion5(){
        return this.question5;
    }

    public BinaryTree getQuestion6(){
        return this.question6;
    }

    public BinaryTree getQuestion7(){
        return this.question7;
    }

    public BinaryTree getQuestion8(){
        return this.question8;
    }

    public BinaryTree getQuestion9(){
        return this.question9;
    }

    public BinaryTree getQuestion10(){
        return this.question10;
    }

    public BinaryTree getQuestion11(){
        return this.question11;
    }

    public BinaryTree getQuestion12(){
        return this.question12;
    }

    public BinaryTree getQuestion13(){
        return this.question13;
    }

    public BinaryTree getQuestion14(){
        return this.question14;
    }

    public BinaryTree getAnswer1(){
        return this.answer1;
    }

    public BinaryTree getAnswer2(){
        return this.answer2;
    }

    public BinaryTree getAnswer3(){
        return this.answer3;
    }

    public BinaryTree getAnswer4(){
        return this.answer4;
    }

    public BinaryTree getAnswer5a(){
        return this.answer5a;
    }

    public BinaryTree getAnswer5b(){
        return this.answer5b;
    }

    public BinaryTree getAnswer6(){
        return this.answer6;
    }

    public BinaryTree getAnswer7(){
        return this.answer7;
    }

    public BinaryTree getAnswer8(){
        return this.answer8;
    }

    public BinaryTree getAnswer9(){
        return this.answer9;
    }

    public BinaryTree getAnswer10(){
        return this.answer10;
    }

    public BinaryTree getAnswer11(){
        return this.answer11;
    }

    public BinaryTree getAnswer12(){
        return this.answer12;
    }

    public BinaryTree getAnswer13(){
        return this.answer13;
    }

    public BinaryTree getAnswer14(){
        return this.answer14;
    }

    // setters
    public void setAns1String(String ans1){
        System.out.println(ans1);
        this.ans1String = ans1;
        this.answer1 = stringToTree(this.ans1String);
        this.ans1String = this.answer1.toString();

    }
    public void setAns2String(String ans2){
        this.ans2String = ans2;
        this.answer2 = stringToTree(this.ans2String);
        this.ans2String = this.answer2.toString();

    }
    public void setAns3String(String ans3){
        this.ans3String = ans3;
        this.answer3 = stringToTree(this.ans3String);
        this.ans3String = this.answer3.toString();

    }
    public void setAns4String(String ans4){
        this.ans4String = ans4;
        this.answer4 = stringToTree(this.ans4String);
        this.ans4String = this.answer4.toString();

    }
    public void setAns5aString(String ans5a){
        this.ans5aString = ans5a;
        this.answer5a = stringToTree(this.ans5aString);
        this.ans5aString = this.answer5a.toString();

    }

    public void setAns5bString(String ans5b){
        this.ans5bString = ans5b;
        this.answer5b = stringToTree(this.ans5bString);
        this.ans5bString = this.answer5b.toString();
    }
    public void setAns6String(String ans6){
        this.ans6String = ans6;
        this.answer6 = stringToTree(this.ans6String);
        this.ans6String = this.answer6.toString();

    }
    public void setAns7String(String ans7){
        this.ans7String = ans7;
        this.answer7 = stringToTree(this.ans7String);
        this.ans7String = this.answer7.toString();

    }
    public void setAns8String(String ans8){
        this.ans8String = ans8;
        this.answer8 = stringToTree(this.ans8String);
        this.ans8String = this.answer8.toString();

    }
    public void setAns9String(String ans9){
        this.ans9String = ans9;
        this.answer9 = stringToTree(this.ans9String);
        this.ans9String = this.answer9.toString();

    }
    public void setAns10String(String ans10){
        this.ans10String = ans10;
        this.answer10 = stringToTree(this.ans10String);
        this.ans10String = this.answer10.toString();

    }
    public void setAns11String(String ans11){
        this.ans11String = ans11;
        this.answer11 = stringToTree(this.ans11String);
        this.ans11String = this.answer11.toString();

    }
    public void setAns12String(String ans12){
        this.ans12String = ans12;
        this.answer12 = stringToTree(this.ans12String);
        this.ans12String = this.answer12.toString();

    }
    public void setAns13String(String ans13){
        this.ans13String = ans13;
        this.answer13 = stringToTree(this.ans13String);
        this.ans13String = this.answer13.toString();
    }
    public void setAns14String(String ans14){
        this.ans14String = ans14;
        this.answer14 = stringToTree(this.ans14String);
        this.ans14String = this.answer14.toString();
    }

    // setting answers
    public void setAnswer1(BinaryTree answer){
        this.answer1 = answer;
        this.ans1String = this.answer1.toString();
    }

    public void setAnswer2(BinaryTree answer){
        this.answer2 = answer;
    }

    public void setAnswer3(BinaryTree answer){
        this.answer3 = answer;
    }

    public void setAnswer4(BinaryTree answer){
        this.answer4 = answer;
    }

    public void setAnswer5(BinaryTree answer[]){
        this.answer5a = answer[0];
        this.answer5b = answer[1];
    }

    public void setAnswer5a(BinaryTree answer){
        this.answer5a = answer;
    }

    public void setAnswer5b(BinaryTree answer){
        this.answer5b = answer;
    }

    public void setAnswer6(BinaryTree answer){
        this.answer6 = answer;
    }

    public void setAnswer7(BinaryTree answer){
        this.answer7 = answer;
    }

    public void setAnswer8(BinaryTree answer){
        this.answer8 = answer;
    }

    public void setAnswer9(BinaryTree answer){
        this.answer9 = answer;
    }

    public void setAnswer10(BinaryTree answer){
        this.answer10 = answer;
    }

    public void setAnswer11(BinaryTree answer){
        this.answer11 = answer;
    }

    public void setAnswer12(BinaryTree answer){
        this.answer12 = answer;
    }

    public void setAnswer13(BinaryTree answer){
        this.answer13 = answer;
    }

    public void setAnswer14(BinaryTree answer){
        this.answer14 = answer;
    }



    public String getAns1String(){
        return this.ans1String;
    }

    public String getAns2String(){
        return this.ans2String;
    }

    public String getAns3String(){
        return this.ans3String;
    }

    public String getAns4String(){
        return this.ans4String;
    }

    public String getAns5aString(){
        return this.ans5aString;
    }

    public String getAns5bString(){
        return this.ans5bString;
    }

    public String getAns6String(){
        return this.ans6String;
    }

    public String getAns7String(){
        return this.ans7String;
    }

    public String getAns8String(){
        return this.ans8String;
    }

    public String getAns9String(){
        return this.ans9String;
    }

    public String getAns10String(){
        return this.ans10String;
    }

    public String getAns11String(){
        return this.ans11String;
    }

    public String getAns12String(){
        return this.ans12String;
    }

    public String getAns13String(){
        return this.ans13String;
    }

    public String getAns14String(){
        return this.ans14String;
    }

    public long getSeed(){
        return this.seed;
    }
}
