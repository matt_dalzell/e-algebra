/*
 * File         : SolvingTree.java
 *
 * Date         : Friday 31 January 2020
 *
 * Description  : Trying to solve a tree, where each of the instances is either a symbol or a number
 *
 * History      : 31/01/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */
/**
 * OLD VERSION FOR THE SIMPLIFICATION OF TREES, NO LONGER USED BUT USEFUL TO SEE A STARTING POINT
 */
/*

//  THIS WAS THE FIRST ATTEMPT AT STANDARDISE TREE


package yp.ealgebra.compiler;

import java.lang.Math;


public class SolvingTree {
    private int answer;

    // default constructor
    public SolvingTree() {
    }

    // method for going through it all
    public static double solve(BinaryTree inputTree){
        double answer = 0;
        BinaryTreeNode root = inputTree.getRoot();
        answer = solveTraversal(root);

        return answer;
    }

    // THIS IS A WAY WHICH ONLY WORKS WHEN ALL OF THE CHILDREN IN THERE ARE INTS
    private static double solveTraversal(BinaryTreeNode node){
        double left = 999, right = 999;
        if(node.hasLeft())
            left = solveTraversal(node.getLeft());
        if(node.hasRight())
            right = solveTraversal(node.getRight());
        // so now it has got both of them, but what if it

        // so now you want to do the logic of it
        if(node.getValue().equals("+"))
            return (left+right);
        if(node.getValue().equals("-"))
            return (left-right);
        if(node.getValue().equals("*") || node.getValue().equals("#"))
            return (left*right);
        if(node.getValue().equals("/"))
            return left/right;
        if(node.getValue().equals("^"))
            return Math.pow(left, right);
        if(node.getValue().equals("U-"))
            return (-1)*left;
        if(node.getValue() instanceof String) {
            return Integer.parseInt(node.getValue().toString());
        }
        return 999999;

    }



    public static double solve(BinaryTreeNode root){
        BinaryTree realTree = new BinaryTree(root);
        return solve(realTree);
    }

    // testing if a string is an instance of an integer, for internal use in this class
    private static boolean isInt(String inString){
        try{
            Integer.parseInt(inString);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    private static String stringMath(String inputVal, int increment, String sign){
        if(isInt(inputVal)){
            int inInt = Integer.parseInt(inputVal);
            // then you want to perform the actual maths
            if(sign.equals("^"))
                inInt = (int)Math.pow(inInt, increment);
            if(sign.equals("#") || sign.equals("*"))
                inInt *= increment;
            else if(sign.equals("/"))
                inInt /= increment;
            else if(sign.equals("-"))
                inInt -= increment;
            else if(sign.equals("+"))
                inInt += increment;
            return (inInt + "");
        }
        else{
            throw new NumberFormatException();
        }
    }

    public static String stringAllMath(String left, String right, String sign){
        if(isInt(left) && isInt(right)){
            int rightInt = Integer.parseInt(right);
            String answer = stringMath(left, rightInt, sign);
            return answer;
        }
        return null;
    }

    // method which is used to initiate the string math
    public static BinaryTreeNode performMath(BinaryTreeNode inNode){
        if(inNode.hasChildren() && !inNode.getLeft().hasChildren() && !inNode.getRight().hasChildren() &&
        isInt(inNode.getLeft().getValue().toString()) && isInt(inNode.getRight().getValue().toString())){
            String answer = stringAllMath(inNode.getLeft().getValue().toString(), inNode.getRight().toString(), inNode.getValue().toString());
            BinaryTreeNode newRoot = new BinaryTreeNode(answer);
            inNode = newRoot;
        }
        return inNode;
    }

    // this is making a way increment a number when it is already a part of a string
    private static String incString(String inputNumber, int incVal){
        if(isInt(inputNumber)){
            int value = Integer.parseInt(inputNumber);
            value += incVal;
            inputNumber = "" + value;
        }
        return inputNumber;
    }


    private static boolean isLetter(String inString){
        if(inString.equals("x") || inString.equals("y") || inString.equals("z"))
            return true;
        return false;
    }


    // now making the method for expansion based on the pseudocode given
    public static BinaryTree expand(BinaryTree tree){
        BinaryTreeNode rootNode = deepCopy(tree.getRoot());
        BinaryTreeNode newRoot = rootNode;
        if(rootNode.hasLeft())
            newRoot.setLeft(expand(new BinaryTree(rootNode.getLeft())));
        if(rootNode.hasRight())
            newRoot.setRight(expand(new BinaryTree(rootNode.getRight())));

        // then actually doing it for the node
        String mult = rootNode.getValue().toString();
        if(mult.equals("*") || mult.equals("#")){
            String add = rootNode.getLeft().getValue().toString();
            if(add.equals("-") || add.equals("+")) {
                // then follow what is in the pseudocode
                BinaryTreeNode nodeCopy = deepCopy(rootNode.getRight());
                BinaryTreeNode tleft = new BinaryTreeNode(rootNode.getLeft().getLeft(), mult,rootNode.getRight());
                BinaryTreeNode tright = new BinaryTreeNode(rootNode.getLeft().getRight(), mult,nodeCopy);
                newRoot = new BinaryTreeNode(tleft, add, tright);
                newRoot.setLeft(expand(new BinaryTree(newRoot.getLeft())));
                newRoot.setRight(expand(new BinaryTree(newRoot.getRight())));
            }
            // then doing the same thing again but for the right tree.
            add = rootNode.getRight().getValue().toString();
            if(add.equals("-") || add.equals("+")){
                // then doing the same thing as above but the other way around
                BinaryTreeNode nodeCopy = deepCopy(rootNode.getLeft());
                BinaryTreeNode tleft = new BinaryTreeNode(rootNode.getLeft(),mult, rootNode.getRight().getLeft());
                BinaryTreeNode tright = new BinaryTreeNode(nodeCopy,mult, rootNode.getRight().getRight());
                newRoot = new BinaryTreeNode(tleft,add, tright);
                newRoot.setLeft(expand(new BinaryTree(newRoot.getLeft())));
                newRoot.setRight(expand(new BinaryTree(newRoot.getRight())));
            }

        }


        return new BinaryTree(newRoot);
    }

    // making the method of deep copy to be able to retain the original tree
    public static BinaryTreeNode deepCopy(BinaryTreeNode input){
        BinaryTreeNode newRoot = new BinaryTreeNode(input.getValue());
        if(input.hasLeft())
            newRoot.setLeft(deepCopy(input.getLeft()));
        if(input.hasRight())
            newRoot.setRight(deepCopy(input.getRight()));
        return newRoot;
    }


    // this is the second try of making it become left associative
    public static BinaryTree makeLeftAssoc(BinaryTree inputTree) {
        BinaryTreeNode inRoot = inputTree.getRoot();
        BinaryTreeNode newRoot;
        String temp;        // will always have a new value to it right before testing
        // so the first thing you do is go to the right and see what that symbol is equal to
        boolean finished = false;
        while (!finished) {
            if (inRoot.getValue().toString().equals("+")) {
                // so when it is a plus
                temp = inRoot.getRight().getValue().toString();
                if (temp.equals("+") || temp.equals("-")) {
                    BinaryTreeNode newLeft = new BinaryTreeNode(inRoot.getLeft(), "+", inRoot.getRight().getLeft());
                    newRoot = new BinaryTreeNode(newLeft, temp, inRoot.getRight().getRight());
                    // running the return within the ifs saves some computation at the end
                    inRoot = newRoot;
                }
                else
                    finished = true;
            }
            else if(inRoot.getValue().toString().equals("-")){
                // then you want to do somewhat similar to before, but with a small difference
                temp = inRoot.getRight().getValue().toString();
                if(temp.equals("+") || temp.equals("-")){
                    BinaryTreeNode newLeft = new BinaryTreeNode(inRoot.getLeft(), "-", inRoot.getRight().getLeft());
                    if(temp.equals("+"))
                        newRoot = new BinaryTreeNode(newLeft, "-", inRoot.getRight().getRight());
                    else
                        newRoot = new BinaryTreeNode(newLeft, "+", inRoot.getRight().getRight());
                    inRoot = newRoot;
                }
                else
                    finished = true;
            }
            else
                finished = true;
        }
        if(inRoot.hasLeft())
            inRoot.setLeft(makeLeftAssoc(new BinaryTree(inRoot.getLeft())));
        return new BinaryTree(inRoot);
    }


    // making some way to put letters on the right when they are in trees
    // so how do you want to do that

    public static BinaryTreeNode lettersRight(BinaryTreeNode inputNode){
        // base case, when they have reached the end, then there is nothing to do
        if(!inputNode.hasChildren()){
           return inputNode;
        }
        else {
            // then you want to do this method on the left and then on the right
            inputNode.setLeft(lettersRight(inputNode.getLeft()));
            inputNode.setRight(lettersRight(inputNode.getRight()));
            // so now you want to check if there is a number on the left
            if (inputNode.getValue().toString().equals("*") || inputNode.getValue().toString().equals("#")) {
                if (isLetter(inputNode.getLeft().getValue().toString())) {
                    if (isInt(inputNode.getRight().getValue().toString())) {
                        // then swap the way around they are
                        BinaryTreeNode tempNode = deepCopy(inputNode.getRight());
                        inputNode.setRight(inputNode.getLeft());
                        inputNode.setLeft(tempNode);
                    }
                    // what if they are both the same letter now
                    else if (inputNode.getLeft().getValue().equals(inputNode.getRight().getValue().toString())) {
                        inputNode.setValue("^");
                        inputNode.setRight(new BinaryTreeNode("2"));
                    }
                }
                // so this is where you want to do the movements of other

                if((inputNode.getLeft().getValue().equals("#") || inputNode.getValue().equals("*")) &&
                !inputNode.getRight().hasChildren() && !inputNode.getLeft().getLeft().hasChildren() &&
                !inputNode.getLeft().getRight().hasChildren()){
                    // then you now want work work through that thing
                    BinaryTreeNode newRoot = new BinaryTreeNode(inputNode.getValue());
                    newRoot.setLeft(deepCopy(inputNode.getLeft().getLeft()));
                    newRoot.setRight(deepCopy(inputNode.getLeft()));
                    newRoot.getRight().setLeft(inputNode.getRight());
                    inputNode = lettersRight(newRoot);
                }
                // incrementing powers when it is more than just squared
                // stopping a null pointer from happeneing
                if(inputNode.hasChildren() && inputNode.getLeft().hasChildren() &&
                        inputNode.getLeft().getRight().hasChildren()) {
                    if (inputNode.getLeft().getRight().getValue().equals("^") &&
                            inputNode.getLeft().getRight().getLeft().getValue().equals(inputNode.getRight().getValue()) &&
                            isInt(inputNode.getLeft().getRight().getRight().getValue().toString())) {
                        // then you want to do the moving for this
                        BinaryTreeNode tempNode = deepCopy(inputNode.getLeft());
                        String newNum = incString(inputNode.getLeft().getRight().getRight().getValue().toString(), 1);
                        tempNode.getRight().getRight().setValue(newNum);
                        inputNode = tempNode;
                    }
                }
            }
        return inputNode;
        }
    }
    public static BinaryTree lettersRight(BinaryTree inputTree){
        BinaryTreeNode tempNode = lettersRight(inputTree.getRoot());
        return new BinaryTree(tempNode);
    }

     How do you want to go about building this? You should make some kind of map for each time you find one of the
     unknowns. Then you can use the silly method from before to be able to find the numbers of the questions
     -  But is it cheating if you just use that?
     - You want this to do something which is similar to what is done in the solve traversal
     - What if you were to split it up into multiple trees, each of them with their own unknown on it, and then use some
       kind of method to put them back together (eg alphabetical and then by their powers)

    public static BinaryTree simplify(BinaryTree inputTree){
        BinaryTreeNode rootNode = inputTree.getRoot();
        rootNode = simplify(rootNode);
        inputTree = new BinaryTree(rootNode);
        return inputTree;

    }

    // you may need to make an edge case for this because # == *
    public static boolean nodeEqual(BinaryTreeNode node1, BinaryTreeNode node2) {
        boolean result = true;
        // then you want to check
        // left
        if (node1.hasLeft()) {
            if (node2.hasLeft()) {
                if (!nodeEqual(node1.getLeft(), node2.getLeft()))
                    result = false;
            } else
                result = false;
        }
        // right
        if (node1.hasRight()) {
            if (node2.hasRight()) {
                if (!nodeEqual(node1.getRight(), node2.getRight()))
                    result = false;
            } else
                result = false;
        }
        // the current node
        if (!node1.getValue().equals(node2.getValue())) {
            if (!((node1.getValue().equals("#") || node1.getValue().equals("*"))
                    && (node2.getValue().equals("#") || node2.getValue().equals("*"))))
                result = false;



        }
        // edge case to allow the different symbol for implicit multiplication to work, this may be removed later
        return result;
    }

    public static boolean treeEqual(BinaryTree tree1, BinaryTree tree2){
        return nodeEqual(tree1.getRoot(), tree2.getRoot());
    }




    // making a simplify method which is able to work with the nodes, this means it can be done reccursively
    public static BinaryTreeNode simplify(BinaryTreeNode rootNode){
        // the base case is when it has to do the left and the right
        if(rootNode.hasRight())
            rootNode.setRight(simplify(rootNode.getRight()));
        if(rootNode.hasLeft())
            rootNode.setLeft(simplify(rootNode.getLeft()));

        if(rootNode.hasChildren()) {
            if (!rootNode.getLeft().hasChildren() && !rootNode.getRight().hasChildren()) {
                // if this is a terminal node
                if (isInt(rootNode.getLeft().getValue().toString()) && isInt(rootNode.getRight().getValue().toString())) {
                    // then you want to put this through the simplify method
                    String result = stringAllMath(rootNode.getLeft().getValue().toString(), rootNode.getRight().getValue().toString(),
                            rootNode.getValue().toString());
                    BinaryTreeNode newRoot = new BinaryTreeNode(result);
                    rootNode = newRoot;
                }
            }
        }

        // doing right and then left because it is a left associative tree

        return rootNode;
    }

    public static BinaryTree simplifyMethods(BinaryTree input){
        input = expand(input);
        input = makeLeftAssoc(input);
        input = lettersRight(input);
        input = simplify(input);
        return input;
    }


}


*/