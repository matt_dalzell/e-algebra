/*
 * File         : Main.java
 *
 * Date         : Thursday 23 January 2020
 *
 * Description  : This is the class for testing everything and making the trees work
 *
 * History      : 23/01/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */
package yp.ealgebra.compiler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;


public class Main {

    public static void main(String[] args) {
        // testing all of the simplify methods which have been made
        String inputString = "55/2 + 42/4" + "\r";

        BinaryTree myTree = new BinaryTree();
        InputStream is = new ByteArrayInputStream( inputString.getBytes() );
        TreeGenerator treeGen = new TreeGenerator(is);
        try{
            myTree = treeGen.Start(System.out);
        }catch(Exception e){
            System.out.println("Exception caught in generation : "+e);
        };
        //System.out.println(myTree);
        myTree = StandardiseTree.standardise(myTree);
        //System.out.println(myTree);
        //System.out.println(myTree.findDepth(myTree.getRoot()));
        //System.out.println(StandardiseTree.treeDepth(myTree));

        // how can you make the tree work, surely the higher up it is, the more you need to make sure it is extended
        // so for example


        /*
        String inputString1 = "(x+3)(x-8)" + "\r";
        String inputString2 = "(x+1)(x-3)" + "\r";

        BinaryTree tree1 = new BinaryTree();

        InputStream is1 = new ByteArrayInputStream(inputString1.getBytes());

        TreeGenerator treeGen1 = new TreeGenerator(is1);

        try{
            tree1 = treeGen1.Start(System.out);

        }
        catch(Exception e){
            System.out.println("Exception caught when building the trees : " + e);
        }
        BinaryTree tree2 = new BinaryTree();
        InputStream is2 = new ByteArrayInputStream(inputString2.getBytes());
        treeGen1.ReInit(is2);
        try{
            tree2 = treeGen1.Start(System.out);
        }
        catch(Exception e){
            System.out.println("Exception caught in building the second tree : " + e);
        }

        tree1 = SolvingTree.expand(tree1);
        tree1 = SolvingTree.simplify(tree1);
        tree2 = SolvingTree.expand(tree2);


        System.out.println(tree1);
        System.out.println(tree2);
        if(SolvingTree.treeEqual(tree1, tree2)){
            System.out.println("This has worked and they are equal");
        }
        */


        /*
        // so you need to make this nice and clean
        String inputString = "(1+x)(3+x)" + "\n";        // all need to end with \r or \n
        // SHOW AN EXAMPLE WHERE THE STRING IS EQUAL TO (x+1)(x-2)^2
        BinaryTree myTree = new BinaryTree();
        InputStream is = new ByteArrayInputStream( inputString.getBytes() );
        TreeGenerator treeGen = new TreeGenerator(is);
        try{
            myTree = treeGen.Start(System.out);
        }catch(Exception e){
            System.out.println("Exception caught in generation : "+e);
        }
        // so now what will you do with putting in the
        //System.out.println(myTree);
        //BinaryTree retTree = SolvingTree.treeMultiply(new BinaryTree(myTree.getRoot().getLeft()), new BinaryTree(myTree.getRoot().getRight()));
        //System.out.println(retTree);
        // so what do you want to do with all things that are in here
        System.out.println(myTree);
        BinaryTree expanded = SolvingTree.expand(myTree);
        System.out.println(expanded);
        expanded = SolvingTree.makeLeftAssoc(expanded);
        System.out.println(expanded);
        //double answer = SolvingTree.solve(expanded);
        //System.out.println(answer);
        // testing the method to simplify it a bit more
        expanded = SolvingTree.lettersRight(expanded);
        System.out.println(expanded);
        expanded = SolvingTree.simplify(expanded);
        System.out.println(expanded);

         */
    }
}
