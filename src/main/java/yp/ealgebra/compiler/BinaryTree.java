/*
 * File         : BinaryTree.java
 *
 * Date         : Thursday 23 January 2020
 *
 * Description  : This is the class for the data structure, this will contain the toString and methods for traversing it
 *
 * History      : 23/01/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */
package yp.ealgebra.compiler;

public class BinaryTree {
    private BinaryTreeNode root;

    // default constructor
    public BinaryTree(){
        this.root = new BinaryTreeNode();
    }

    // constructing when you already have a given root
    public BinaryTree(BinaryTreeNode root){
        this.root = root;
    }

    // this was an edge case for making the compiler work more easily, you were able to put in the nodes and a new op
    public BinaryTree(BinaryTree left, BinaryTree right, String operator){
        this.root = new BinaryTreeNode(operator);
        this.root.setLeft(left);
        this.root.setRight(right);
    }

    // traversals for the string - this is depth first, which can produce RPN/postfix
    public String dfTraversal(BinaryTreeNode node, String traversal){
        if(node.hasLeft())
            traversal = dfTraversal(node.getLeft(), traversal);
        if(node.hasRight())
            traversal = dfTraversal(node.getRight(), traversal);

        traversal = traversal + " " + node.getValue();
        return traversal;
    }

    // allows a simple string to be made from the tree, infix/natural format
    public String ioTraversal(BinaryTreeNode node, String traversal, int parentPriority) {
        int myPriority = 0;
        if (node.hasChildren()) {
            myPriority = (int) BinaryTreeNode.priority.get(node.getValue().toString());
            if (myPriority < parentPriority)
                traversal = traversal + "(";
            // then you just need to do what it was doing before
            if (node.hasLeft())
                traversal = ioTraversal(node.getLeft(), traversal, myPriority);
            traversal = traversal + node.getValue() + " ";
            if (node.hasRight())
                traversal = ioTraversal(node.getRight(), traversal, myPriority);
            if (myPriority < parentPriority)
                traversal = traversal + ")";
        }
        else if(node.hasRight()){
            traversal = traversal + node.getValue() + " ";
            traversal = ioTraversal(node.getRight(), traversal, myPriority);

        }
        else
            traversal = traversal + node.getValue() + " ";
        return traversal;
    }

    // puts a binary tree into a "latex string" which can work with MathJax
    // by latex string it means a string which is formatted for latex styling
    public String latexIoTraversal(BinaryTreeNode node, String traversal, int parentPriority){
        int myPriority = 0;
        if (node.hasChildren()) {
            myPriority = (int) BinaryTreeNode.priority.get(node.getValue().toString());
            if(node.getValue().toString().equals("/")){
                if(myPriority < parentPriority)
                    traversal = traversal + "(";
                traversal = traversal + "\\frac{";
                traversal = latexIoTraversal(node.getLeft(), traversal, myPriority) + "}";
                traversal = traversal + "{";
                traversal = latexIoTraversal(node.getRight(), traversal, myPriority) +"}";
                if(myPriority < parentPriority)
                    traversal = traversal + ")";

            }
            else {
                if (myPriority < parentPriority)
                    traversal = traversal + "(";
                // then you just need to do what it was doing before
                if (node.hasLeft())
                    traversal = latexIoTraversal(node.getLeft(), traversal, myPriority);
                if(node.getValue().toString().equals("*")){
                    traversal = traversal + "\\times ";
                }
                else {
                    traversal = traversal + node.getValue() + " ";
                }
                if (node.hasRight()) {
                    if (node.getValue().toString().equals("^")) {
                        traversal = traversal + "{";
                        traversal = latexIoTraversal(node.getRight(), traversal, myPriority) + "}";
                    } else
                        traversal = latexIoTraversal(node.getRight(), traversal, myPriority);
                }
            }
            if (myPriority < parentPriority)
                traversal = traversal + ")";
        }
        else if(node.hasRight()){
            if(node.getValue().toString().equals("*")){
                traversal = traversal + "\\times ";
            }
            else {
                traversal = traversal + node.getValue() + " ";
            }
            traversal = latexIoTraversal(node.getRight(), traversal, myPriority);

        }
        else {
            if(node.getValue().toString().equals("*")){
                traversal = traversal + "\\times ";
            }
            else
                traversal = traversal + node.getValue() + " ";
        }

        return traversal;
    }


    // multiply?
    // should there be a method which is used to multiply two trees together?

    // the string when you pass it into the compiler







    // finding the depth of a tree without requiring it to be left associative
    public int findDepth(BinaryTreeNode root){
        int leftDepth =1;
        int rightDepth=1;
        if(root.hasLeft()){
            leftDepth += findDepth(root.getLeft());
        }
        if(root.hasRight()){
            rightDepth += findDepth(root.getRight());
        }
        return Math.max(leftDepth, rightDepth);


    }

    // comparing the tree to another one, or to a root
 /*   public boolean compare(BinaryTree otherTree){
        BinaryTreeNode tempNode = StandardiseTree.deepCopy(this.root);
        BinaryTreeNode compTo = StandardiseTree.deepCopy(otherTree.getRoot());
        BinaryTreeNode newRoot = new BinaryTreeNode("-");
        newRoot.setLeft(tempNode);
        newRoot.setRight(compTo);
        BinaryTree newTree = new BinaryTree(newRoot);

        newTree = StandardiseTree.standardise(newTree);
        // printing out the inputted trees
        System.out.println("This item = "  + this.toString());
        System.out.println("The compared to = " + otherTree.toString());
        // printing out our tree from the result
        System.out.println("Compare result = " + newTree.toString());
        if(newTree.getRoot().getValue().toString().equals("0") && !newTree.getRoot().hasLeft() &&
                !newTree.getRoot().hasRight())
            return true;
        return false;
    }
*/

    // compares this tree to another, this is used for checking questions
    // the way this works by minusing "otherTree" from this tree
    public boolean compare(BinaryTree otherTree){
        otherTree = StandardiseTree.standardise(otherTree);
        BinaryTreeNode thisTreeRoot = StandardiseTree.deepCopy(this.root);
        BinaryTree compTo = new BinaryTree(thisTreeRoot);
        compTo = StandardiseTree.standardise(compTo);

        // then we want to actually compare them
        BinaryTreeNode compTreeRoot = new BinaryTreeNode("-");
        compTreeRoot.setLeft(compTo);
        compTreeRoot.setRight(otherTree);
        BinaryTree compTree = new BinaryTree(compTreeRoot);
        compTree = StandardiseTree.standardise(compTree);
        return compTree.getRoot().getValue().toString().equals("0") && !compTree.getRoot().hasChildren();


    }


    // allowing to compare to a node just in case
    public boolean compare(BinaryTreeNode root){
        BinaryTree otherTree = new BinaryTree(root);
        return compare(otherTree);
    }

    // sometimes required for manipulation
    public BinaryTreeNode getRoot(){
        return this.root;
    }

    // replacing the root, may have to be done when the empty constructor was used
    public void setRoot(BinaryTreeNode newRoot){
        this.root = newRoot;
    }

    // infix string, toString fits standards more
    public String toString(){
        String placeHolder = "";     // initialising the string for the recursion
        //String output = dfTraversal(this.root, placeHolder);      // produces RPN
        return ioTraversal(this.root, placeHolder, 0);
    }

    // gives a postfix string
    public String toRPNString(){
        String placeHolder = "";
        return dfTraversal(this.root, placeHolder);
    }

    // this is the main one used, this gets done for
    public String latexToString(){
        // so what else do you need to do in here other than what is done in String.
        String placeholder = "";
        // so what do you need to do with this to make it work in the latex way?
        return latexIoTraversal(this.root, placeholder, 0);
    }

}
