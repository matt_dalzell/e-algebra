/*
 * File         : StandardiseTree.java
 *
 * Date         : Saturday 07 March 2020
 *
 * Description  : Making a method to standardise the trees, SolvingTree was a first attempt at this
 *
 * History      : 07/03/2020   --v1.00
 *
 * Author       : Matthew Dalzell   .
 */
package yp.ealgebra.compiler;

import yp.ealgebra.questiongen.Fraction;

import java.util.ArrayList;

public class StandardiseTree {
    // runs all of the key methods
    // this is the main key way in which things will interact with this

    public static BinaryTree standardise(BinaryTree input){
        BinaryTreeNode root = input.getRoot();
        root = minusUp(root);
        root = expand(root);
        root = mla(root);
        root = minusUp(root);
        root = lettersRight(root);
        root = multNodes(root);
        root = powSimp(root);
        root = nodeMath(root);
        root = lettersRight(root);
        root = powGroup(root);
        return new BinaryTree(root);
    }

    // A method used for testing
    public static BinaryTree standardise2(BinaryTree input){
        BinaryTreeNode root = input.getRoot();
        System.out.println("Input");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = minusUp(root);
        System.out.println("Minus up");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = expand(root);
        System.out.println("Expand");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = mla(root);
        System.out.println("MLA");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = minusUp(root);
        System.out.println("Minus Up");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = lettersRight(root);
        System.out.println("Letters Right");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = multNodes(root);
        System.out.println("MultNodes");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = powSimp(root);
        System.out.println("PowSimp");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = nodeMath(root);
        System.out.println("NodeMath");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = lettersRight(root);      // checking they are in the same place after that was done
        System.out.println("LettersRight");
        System.out.println((new BinaryTree(root)).toRPNString());
        root = powGroup(root);
        System.out.println("PowGroup");
        System.out.println((new BinaryTree(root)).toRPNString());
        return new BinaryTree(root);
    }




    // --- MAKING THE METHODS TO BE USED BY STANDARDISE ---
    // moving u-s up, also simplifying cases of a + (-b) into simply a - b
    private static BinaryTreeNode minusUp(BinaryTreeNode root){
        // you want to run this reccursively?
        if(!root.hasChildren())
            return root;
        if(root.hasLeft())
            root.setLeft(minusUp(root.getLeft()));
        if(root.hasRight())
            root.setRight(minusUp(root.getRight()));
        // then there are the things here that you want to do to if
        if(root.getValue().toString().equals("/")){
            if(root.getLeft().getValue().equals("-")){
                // then we need to move this up
                BinaryTreeNode newRoot = new BinaryTreeNode("-");
                BinaryTreeNode newRight = new BinaryTreeNode("/");
                newRight.setRight(deepCopy(root.getRight()));
                newRight.setLeft(deepCopy(root.getLeft().getRight()));
                newRoot.setRight(newRight);
                root = newRoot;
            }
        }
        else if(root.getValue().toString().equals("+")){
            if(root.getRight().getValue().toString().equals("-")){
                root.setValue("-");
                root.setRight(deepCopy(root.getRight().getRight()));
            }
        }
        else if(root.getValue().toString().equals("-") && !root.getRight().hasLeft()){
            if(root.getRight().getValue().toString().equals("-")){
                root.setValue("+");
                root.setRight(deepCopy(root.getRight().getRight()));
            }
        }
        return root;

    }

    // deals with any implicit multiplication
    private static BinaryTreeNode expand(BinaryTreeNode root){
        BinaryTreeNode rootNode = deepCopy(root);
        BinaryTreeNode newRoot = rootNode;
        if(rootNode.hasLeft())
            newRoot.setLeft(expand(rootNode.getLeft()));
        if(rootNode.hasRight())
            newRoot.setRight(expand(rootNode.getRight()));

        // then actually doing it for the node
        String mult = rootNode.getValue().toString();
        if(mult.equals("*")){
            String add = rootNode.getLeft().getValue().toString();
            if((add.equals("-") && rootNode.getLeft().hasChildren()) || add.equals("+")) {
                // then follow what is in the pseudocode
                BinaryTreeNode nodeCopy = deepCopy(rootNode.getRight());
                // gets the left and the right subtrees
                BinaryTreeNode tleft = new BinaryTreeNode(rootNode.getLeft().getLeft(), mult,rootNode.getRight());
                BinaryTreeNode tright = new BinaryTreeNode(rootNode.getLeft().getRight(), mult,nodeCopy);
                newRoot = new BinaryTreeNode(tleft, add, tright);
                newRoot.setLeft(expand(new BinaryTree(newRoot.getLeft())));
                newRoot.setRight(expand(new BinaryTree(newRoot.getRight())));
            }
            // then doing the same thing again but for the right tree.
            add = rootNode.getRight().getValue().toString();
            if(add.equals("-") || add.equals("+")){
                // then doing the same thing as above but the other way around
                BinaryTreeNode nodeCopy = deepCopy(rootNode.getLeft());
                BinaryTreeNode tleft = new BinaryTreeNode(rootNode.getLeft(),mult, rootNode.getRight().getLeft());
                BinaryTreeNode tright = new BinaryTreeNode(nodeCopy,mult, rootNode.getRight().getRight());
                newRoot = new BinaryTreeNode(tleft,add, tright);
                newRoot.setLeft(expand(new BinaryTree(newRoot.getLeft())));
                newRoot.setRight(expand(new BinaryTree(newRoot.getRight())));
            }
        }
        return newRoot;
    }


    // making the tree be left associative
    private static BinaryTreeNode mla(BinaryTreeNode root){
        BinaryTreeNode newRoot;
        String temp;        // will always have a new value to it right before testing
        // so the first thing you do is go to the right and see what that symbol is equal to
        boolean finished = false;
        while (!finished) {
            if (root.getValue().toString().equals("+")) {
                // so when it is a plus
                temp = root.getRight().getValue().toString();
                if (temp.equals("+") || temp.equals("-")) {
                    BinaryTreeNode newLeft = new BinaryTreeNode(root.getLeft(), "+", root.getRight().getLeft());
                    newRoot = new BinaryTreeNode(newLeft, temp, root.getRight().getRight());
                    // running the return within the ifs saves some computation at the end
                    root = newRoot;
                }
                else
                    finished = true;
            }
            else if(root.getValue().toString().equals("-")){
                // then you want to do somewhat similar to before, but with a small difference
                temp = root.getRight().getValue().toString();
                if(temp.equals("+") || temp.equals("-")){
                    BinaryTreeNode newLeft = new BinaryTreeNode(root.getLeft(), "-", root.getRight().getLeft());
                    if(temp.equals("+"))
                        newRoot = new BinaryTreeNode(newLeft, "-", root.getRight().getRight());
                    else
                        newRoot = new BinaryTreeNode(newLeft, "+", root.getRight().getRight());
                    root = newRoot;
                }
                else
                    finished = true;
            }
            else
                finished = true;
        }
        if(root.hasLeft())
            root.setLeft(mla(new BinaryTree(root.getLeft())));
        return root;
    }



    // putting all of the letters on the right of their respecitve nodes
    private static BinaryTreeNode lettersRight(BinaryTreeNode root){
        // when it has hit a u- at the bottom
        if(root.hasRight() && !root.hasChildren() && root.getValue().toString().equals("-")){
            root.setRight(lettersRight(root.getRight()));
            return root;
        }
        // when it does not have anything below it
        else if(isNumOrLetter(root)){
            return root;
        }
        else {
            // then you want to do this method on the left and then on the right
            root.setLeft(lettersRight(root.getLeft()));
            root.setRight(lettersRight(root.getRight()));
            // so now you want to check if there is a number on the left
            if (root.getValue().toString().equals("*")) {
                if (isLetter(root.getLeft().getValue().toString())) {
                    if (isNum(root.getRight())) {
                        // then swap the way around they are
                        BinaryTreeNode tempNode = deepCopy(root.getRight());
                        root.setRight(root.getLeft());
                        root.setLeft(tempNode);
                    }
                    // what if they are both the same letter now
                    else if (root.getLeft().getValue().toString().equals(root.getRight().getValue().toString())) {
                        root.setValue("^");
                        root.setRight(new BinaryTreeNode("2"));
                    }
                }
                // so this is where you want to do the movements of other
                if((root.getValue().toString().equals("*")) && root.getLeft().hasChildren() && !isNum(root.getLeft())
                        && !root.getRight().hasChildren() && !root.getLeft().getLeft().hasChildren() &&
                        !root.getLeft().getRight().hasChildren()){
                    BinaryTreeNode newRoot = new BinaryTreeNode(root.getValue().toString());
                    // then you want to swap the left and the right around
                    newRoot.setLeft(deepCopy(root.getRight()));
                    newRoot.setRight(deepCopy(root.getLeft()));
                    root = lettersRight(newRoot);
                }
                // incrementing powers when it is more than just squared
                if(root.hasChildren() && root.getLeft().hasChildren() &&
                        root.getLeft().getRight().hasChildren()) {
                    if (root.getLeft().getRight().getValue().toString().equals("^") &&
                            root.getLeft().getRight().getLeft().getValue().toString().equals(root.getRight().getValue()) &&
                            isInt(root.getLeft().getRight().getRight().getValue().toString())) {
                        // then you want to do the moving for this
                        BinaryTreeNode tempNode = deepCopy(root.getLeft());
                        //int newInt = stringMath(inputNode.getLeft().getRight().getRight().getValue().toString(), 1,"+");
                        Fraction temp = new Fraction(1,1);
                        Fraction newFract = fractMath(root.getLeft().getRight().getRight(), temp, "+");
                        newFract.simplify();
                        if(newFract.getNumerator() < 0){
                            // then you want to make it a U- node
                            BinaryTreeNode newTemp = new BinaryTreeNode("-");
                            newFract = newFract.mult(-1);
                            newTemp.setRight(newFract.toNode());
                            tempNode.getRight().setRight(newTemp);
                        }
                        else {
                            tempNode.getRight().setRight(newFract.toNode());
                        }
                        root = tempNode;
                    }
                }
            }
            return root;
        }
    }

    // simplifying nodes when you have two multiplied together
    private static BinaryTreeNode multNodes(BinaryTreeNode root){
        if(root.hasLeft() && !isNumOrLetter(root))
            root.setLeft(multNodes(root.getLeft()));
        if(root.hasRight() && !isNumOrLetter(root))
            root.setRight(multNodes(root.getRight()));
        // then you want to work out what you are going to do with the node when you have been given it
        if(root.getValue().toString().equals("*") && isNum(root.getLeft()) && isNum(root.getRight())){
            // then we want to multiply these
            // getting the fractions
            Fraction leftFract = nodeMult(root.getLeft());
            Fraction rightFract = nodeMult(root.getRight());
            // getting the powers
            int leftPower = nodePower(root.getLeft());
            int rightPower = nodePower(root.getRight());
            // then we want to make the new nodes
            Fraction fractResult = leftFract.mult(rightFract);
            int powerResult = leftPower + rightPower;
            // then we want to make that into a node
            root = splitToNode(fractResult, powerResult);
        }
        return root;
    }

    // doing the simple math to trees
    private static BinaryTreeNode nodeMath(BinaryTreeNode root){
        if(root.hasChildren()) {
            if (root.getLeft().hasChildren())
                root.setLeft(nodeMath(root.getLeft()));
            if (root.getRight().hasChildren())
                root.setRight(nodeMath(root.getRight()));
            // then you want to try and do the maths on the node
            if (isNum(root.getLeft()) && isNum(root.getRight())) {
                // then you want to find out what the operation is that you are now doing on this
                // times
                switch (root.getValue().toString()) {
                    case "+": {
                        Fraction left = nodeMult(root.getLeft());
                        Fraction right = nodeMult(root.getRight());
                        Fraction result = left.add(right);
                        result.simplify();
                        root = result.toNode();
                        break;
                    }
                    case "-": {
                        Fraction left = nodeMult(root.getLeft());
                        Fraction right = nodeMult(root.getRight());
                        Fraction result = left.sub(right);
                        result.simplify();
                        if (result.getNumerator() < 0) {
                            result = result.mult(-1);
                            root = new BinaryTreeNode("-");
                            root.setRight(result.toNode());

                        } else {
                            root = result.toNode();
                        }
                        break;
                    }
                    // when you need to divide two fractions, this is not a great idea to rely on, but none the less is necessary
                    case "/": {
                        // but then when it finds a fraction it needs to find out
                        Fraction left = nodeMult(root.getLeft());
                        Fraction right = nodeMult(root.getRight());
                        Fraction result = left.div(right);
                        result.simplify();
                        root = result.toNode();
                        break;
                    }
                    case "*": {
                        Fraction left = nodeMult(root.getLeft());
                        Fraction right = nodeMult(root.getRight());
                        Fraction result = left.mult(right);
                        result.simplify();
                        root = result.toNode();
                        break;
                    }
                    case "^": {
                        Fraction left = nodeMult(root.getLeft());
                        int right = Integer.parseInt(root.getRight().getValue().toString());
                        Fraction result = left.pow(right);
                        result.simplify();
                        root = result.toNode();
                        break;
                    }
                }
            }
        }
        // gives back any of the changes, or simply that which was passed in
        return root;
    }

    // simplifying all of the powers, this means it can combine them together so you dont have multiple instances on the
    // same power
    private static BinaryTreeNode powSimp(BinaryTreeNode root){
        // but then we do need to make some changes
        if(root.hasLeft()){
            root.setLeft(powSimp(root.getLeft()));
        }
        if(root.hasRight()){
            root.setRight(powSimp(root.getRight()));
        }
        // then we have to work out what we are doing with this one
        if(root.getValue().toString().equals("*") && !isTerminal(root)){
            // then we have something to do with this, we see if we can combine the left and the right
            int leftPower = nodePower(root.getLeft());
            int rightPower = nodePower(root.getRight());
            Fraction leftFract = nodeMult(root.getLeft());
            Fraction rightFract = nodeMult(root.getRight());
            // then we do the combinations we are required to do
            Fraction finalFraction = leftFract.mult(rightFract);
            int finalPower = leftPower + rightPower;
            root = splitToNode(finalFraction, finalPower);
        }
        return root;
    }

    // this takes the tree, sorts all of the nodes into the right order, and removes any which are equal to zero,
    // if the final tree is just equal to zero, that is what it returns
    // prev version was able to use sub-functions, but since changing it to fractions containing more data structures,
    // that is no longer possible
    private static BinaryTreeNode powGroup(BinaryTreeNode root) {
        BinaryTreeNode newRoot = new BinaryTreeNode("0");
        if (treeDepth(root) == 0) {
            if (root.hasLeft() && root.getLeft().getValue().toString().equals("0")) {
                return newRoot;
            }
            return root;
        }
        else {
            ArrayList<Fraction> degrees = new ArrayList<>();
            ArrayList<Integer> powers = new ArrayList<>();
            BinaryTreeNode currentTemp = root;
            for(int i = 0; i < treeDepth(root); i++){
                Fraction tempMult = nodeMult(currentTemp.getRight());
                tempMult.simplify();
                int tempPower = nodePower(currentTemp.getRight());
                if(currentTemp.getValue().toString().equals("-"))
                    tempMult = tempMult.mult(-1);
                tempMult.simplify();
                // now that you have made these values, you need to work out where they go in their arrays
                if(degrees.size() == 0){
                    // then this is when we just put the values in
                    degrees.add(tempMult);
                    powers.add(tempPower);
                }
                else{
                    // we need to work out where in there that it goes
                    int j = 0;
                    boolean found = false;
                    while(j < degrees.size() && !found){
                        // then we want to work out what to do with it
                        if(tempPower > powers.get(j)){
                            j++;
                        }
                        else if(tempPower == powers.get(j)){
                            Fraction tempFrac = degrees.get(j);
                            tempFrac = tempFrac.add(tempMult);
                            degrees.set(j, tempFrac);
                            degrees.get(j).simplify();
                            found = true;
                        }
                        else{
                            // then this means it needs to push the rest up and place this one in the current location
                            degrees.add(j, tempMult);
                            powers.add(j, tempPower);
                            found = true;
                        }
                    }
                    if(!found){
                        degrees.add(tempMult);
                        powers.add(tempPower);
                    }
                }
                currentTemp = currentTemp.getLeft();
            }
            // now you need to work out what to do with that one which is hanging low to the left
            int finalPower;
            Fraction finalFraction;
            // so the same things again but now we know the node
            boolean negative = false;
            if(currentTemp.getValue().toString().equals("-")) {
                negative = true;
                currentTemp = currentTemp.getRight();
            }
            // then you want to do the things you have done before to work this out
            finalPower = nodePower(currentTemp);
            finalFraction = nodeMult(currentTemp);
            if(negative) {
                finalFraction = finalFraction.mult(-1);
            }
            // then you have to work out where this will fit
            if(degrees.size() == 0){
                degrees.add(finalFraction);
                powers.add(finalPower);
            }
            else{
                // then you need to do the same loop as before on this
                int i = 0;
                boolean found = false;
                while(i < degrees.size()&& !found){
                    if(finalPower > powers.get(i))
                        i++;
                    else if(finalPower == powers.get(i)){
                        //System.out.println(degrees.get(i) + " and " + finalFraction);
                        Fraction temp = degrees.get(i).add(finalFraction);
                        degrees.set(i, temp);
                        found = true;
                    }
                    else{
                        // then we have found the location where we have to insert these
                        degrees.add(i, finalFraction);
                        powers.add(i, finalPower);
                        found = true;
                    }
                }
                if(!found){
                    degrees.add(finalFraction);
                    powers.add(finalPower);
                }
            }
            // simplifying all of the fractions down now
            for (Fraction degree : degrees) degree.simplify();
            BinaryTreeNode finalRoot = new BinaryTreeNode();
            int count = 0;
            boolean placed = false;
            Fraction zero = new Fraction();
            zero.setNumerator(0);
            zero.setDenominator(1);
            while(!placed && count < degrees.size()){
                if(degrees.get(count).equalsZero())
                    count++;
                else{
                    finalRoot = splitToNode(degrees.get(count), powers.get(count));
                    count++;
                    placed = true;
                }
            }
            // now you want to keep going from that location
            for(int i = count; i < degrees.size(); i++){
                if(!degrees.get(i).equalsZero()){
                    BinaryTreeNode temp = deepCopy(finalRoot);
                    if(degrees.get(i).getNumerator() > 0){
                        finalRoot = new BinaryTreeNode("+");
                        finalRoot.setRight(splitToNode(degrees.get(i), powers.get(i)));
                    }
                    else{
                        //System.out.println(degrees.get(i) + " and " + powers.get(i));
                        finalRoot = new BinaryTreeNode("-");
                        Fraction tempDegree = degrees.get(i);
                        tempDegree = tempDegree.mult(-1);
                        finalRoot.setRight(splitToNode(tempDegree, powers.get(i)));
                    }
                    finalRoot.setLeft(temp);
                    placed = true;
                }
            }
            // then when you have ended up at the end
            if(!placed)
                finalRoot = new BinaryTreeNode(0+"");
            return finalRoot;
        }
    }


    // --- MAKING THE SUPPORTING METHODS ---
    // checking of the string passed in is an integer
    public static boolean isInt(String inString){
        try{
            Integer.parseInt(inString);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }

    public static boolean isNum(BinaryTreeNode node){
        // then we want to see if it is either a number or a fraction
        if(isInt(node.getValue().toString()))
            return true;
        if(node.getValue().toString().equals("/") && isInt(node.getLeft().getValue().toString()) && isInt(node.getRight().getValue().toString()))
            return true;
        else return node.getValue().toString().equals("-") && node.getRight().getValue().toString().equals("/") &&
                isInt(node.getRight().getLeft().getValue().toString()) &&
                isInt(node.getRight().getRight().getValue().toString());
    }

    // checking if the node is a letter of it is actually a number/symbol. Has own method because it is so commonly used
    private static boolean isLetter(String inString){
        return inString.equals("x");
    }

    // checking if a string being passed in is a letter

    // making the deep copy method to copy trees without just being references
    public static BinaryTreeNode deepCopy(BinaryTreeNode input){
        BinaryTreeNode newRoot = new BinaryTreeNode(input.getValue());
        if(input.hasLeft())
            newRoot.setLeft(deepCopy(input.getLeft()));
        if(input.hasRight())
            newRoot.setRight(deepCopy(input.getRight()));
        return newRoot;
    }


    // making an improved version of the previous string math, when it is dealing with an integer, but this can
    // manipulate fractions with usual maths symbols
    private static Fraction fractMath(BinaryTreeNode node, Fraction increment, String sign){
        if(isNum(node)) {
            Fraction inFract = nodeMult(node);
            inFract.simplify();
            increment.simplify();
            switch (sign) {
                case "^":
                    if (increment.getDenominator() != 1) {
                        throw new NumberFormatException();
                    } else {
                        inFract = inFract.pow(increment.getNumerator());
                    }
                    break;
                case "*":
                    inFract = inFract.mult(increment);
                    break;
                case "/":
                    inFract = inFract.div(increment);
                    break;
                case "-":
                    inFract = inFract.sub(increment);
                    break;
                case "+":
                    inFract = inFract.add(increment);
                    break;
            }
            return inFract;
        }
        else{
            throw new NumberFormatException();
        }
    }

    // turning Fraction + Power into a node
    private static BinaryTreeNode splitToNode(Fraction fraction, int power) {
        BinaryTreeNode output = new BinaryTreeNode("0");
        fraction.simplify();
        // then working out what has to go on with the node
        if(power == 0){
            if(fraction.getNumerator() == 0){
                return new BinaryTreeNode("0");

            }
            else{
                output = fraction.toNode();
            }
        }
        else if(power == 1){
            // then we have to deal with x as well
            if(fraction.getNumerator() == 0){
                return new BinaryTreeNode("0");
            }
            else if(fraction.getNumerator() == fraction.getDenominator()){
                output = new BinaryTreeNode("x");
            }
            else{
                // then this is the multiplier and then the value
                output = new BinaryTreeNode("*");
                output.setLeft(fraction.toNode());
                output.setRight(new BinaryTreeNode("x"));
            }
        }
        else{
            // then this means power is going to have to be dealt with
            if(fraction.getNumerator() == 0){
                // then we return 0 again
                return new BinaryTreeNode("0");
            }
            else if(fraction.getNumerator() == fraction.getDenominator()){
                // then this is 1 * that power
                output = new BinaryTreeNode("^");
                output.setLeft(new BinaryTreeNode("x"));
                output.setRight(new BinaryTreeNode(power + ""));
            }
            else{
                // then this is going to have to be the full thing
                output = new BinaryTreeNode("*");
                output.setLeft(fraction.toNode());
                BinaryTreeNode tempRight = new BinaryTreeNode("^");
                tempRight.setLeft(new BinaryTreeNode("x"));
                tempRight.setRight(new BinaryTreeNode(power + ""));
                output.setRight(tempRight);
            }
        }
        return output;
    }

    // works out how far down a tree goes, to be able to process them iteratively rather than recursively ONLY LEFT ASSOC
    public static int treeDepth(BinaryTree input){
        BinaryTreeNode root = input.getRoot();
        int count = 0;
        while(isParent(root)){
            count = count + 1;
            root = root.getLeft();
        }
        return count;
    }

    // this made sense to put in it's own method because it was being used more than one time
    private static boolean isParent(BinaryTreeNode inputNode){
        return inputNode.hasChildren() && (inputNode.getValue().toString().equals("+") || inputNode.getValue().toString().equals("-"));
    }

    private static Fraction nodeMult(BinaryTreeNode node){
        //System.out.println("Node const says : " + new BinaryTree(node));
        Fraction output = new Fraction();
        boolean neg = false;
        if(node.getValue().toString().equals("-")) {
            neg = true;
            node = node.getRight();
        }
        if(isLetter(node.getValue().toString())){
            output.setNumerator(1);
            output.setDenominator(1);
            //return output;
        }
        else if(isInt(node.getValue().toString())){
            output.setNumerator(Integer.parseInt(node.getValue().toString()));
            output.setDenominator(1);
            //return output;
        }
        else if(node.getValue().toString().equals("/")){
            // then this is nice and easy, it is a fraction
            output.setNumerator(Integer.parseInt(node.getLeft().getValue().toString()));
            output.setDenominator(Integer.parseInt(node.getRight().getValue().toString()));
            //return output;
        }
        else if(node.getValue().toString().equals("*")){
            // then what if we pass the one on the LEFT into this!
            output = nodeMult(node.getLeft());
            return output;
        }
        else if(node.getValue().toString().equals("^")){
            output = nodeMult(node.getLeft());
            return output;
        }
        else {
            // error values to look out for
            output.setNumerator(50000);
            output.setDenominator(999999);
        }
        if(neg){
            output = output.mult(-1);
        }
        output.simplify();
        return output;

    }

    // now you want to make a new one for this, because the previous was clearly a mess
    private static int nodePower(BinaryTreeNode node){
        if(node.getValue().toString().equals("-") && !node.hasLeft())
            return nodePower(node.getRight());
        if(isLetter(node.getValue().toString())){
            return 1;
        }
        if(node.getValue().toString().equals("/") || isInt(node.getValue().toString())){
            // then it is just a number
            return 0;
        }
        else{
            if(node.getValue().toString().equals("^")){
                return Integer.parseInt(node.getRight().getValue().toString());
            }
            else if(node.getValue().toString().equals("*")){
                if(node.getRight().toString().equals("^"))
                    return Integer.parseInt(node.getRight().getRight().getValue().toString());
                else
                    return 1;
            }
        }
        // error output to look out for
        return 888;
    }

    // checking if we are at a node which is at the bottom of a tree, eg has no children
    private static boolean isTerminal(BinaryTreeNode node){
        if(!node.hasLeft() && node.hasRight())
            return true;
        return node.getValue().toString().equals("*") && isNumOrLetter(node.getRight()) && isNumOrLetter(node.getLeft());
    }

    private static boolean isNumOrLetter(BinaryTreeNode node){
        // so we want to see if it is a letter or if it is a number
        if(isNum(node))
            return true;
        return isLetter(node.getValue().toString());
    }


    // --- MAKING METHODS WORK WITH BINARYTREE WHICH HAVE ALWAYS BEEN DESIGNED TO BE BINARYTREENODE, mainly for testing
    private static BinaryTree expand(BinaryTree inputTree){
        BinaryTreeNode root = inputTree.getRoot();
        root = expand(root);
        return new BinaryTree(root);
    }

    private static BinaryTree mla(BinaryTree inputTree){
        BinaryTreeNode root = inputTree.getRoot();
        root = mla(root);
        return new BinaryTree(root);
    }

    private static BinaryTree lettersRight(BinaryTree inputTree){
        BinaryTreeNode root = inputTree.getRoot();
        root = lettersRight(root);
        return new BinaryTree(root);
    }


    private static BinaryTree powGroup(BinaryTree input){
        BinaryTreeNode root = input.getRoot();
        root = powGroup(root);
        input.setRoot(root);
        return input;
    }

    private static BinaryTree nodeMath(BinaryTree input){
        BinaryTreeNode root = input.getRoot();
        root = nodeMath(root);
        input.setRoot(root);
        return input;
    }

    private static BinaryTree powSimp(BinaryTree input){
        BinaryTreeNode root = input.getRoot();
        root = powSimp(root);
        input.setRoot(root);
        return input;
    }


    private static BinaryTree minusUp(BinaryTree inputTree){
        BinaryTreeNode root = inputTree.getRoot();
        root = minusUp(root);
        inputTree.setRoot(root);
        return inputTree;
    }

    private static BinaryTree multNodes(BinaryTree inputTree){
        BinaryTreeNode root = inputTree.getRoot();
        root = multNodes(root);
        inputTree.setRoot(root);
        return inputTree;
    }

    private static int treeDepth(BinaryTreeNode root){
        BinaryTree tree = new BinaryTree();
        tree.setRoot(root);
        return treeDepth(tree);
    }
}
