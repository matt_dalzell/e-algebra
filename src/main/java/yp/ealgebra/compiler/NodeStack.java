/*
 * File         : NodeStack.java
 *
 * Date         : Thursday 23 January 2020
 *
 * Description  : This may not be used, but this is the node stack to aid with tree traversal
 *
 * History      : 23/01/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */

// THIS ONE WAS FOR AN EARLY VERSION OF TRAVERSING THE TREE, IS NO LONGER USED BUT WORTH NOTING
/*
package yp.ealgebra.compiler;

import java.util.ArrayList;

public class NodeStack {
    // want to make a new list of nodes
    private ArrayList<BinaryTreeNode> theStack;

    public NodeStack(){
        this.theStack = new ArrayList<>();
    }

    // so you push and pop to/from it


    public boolean isEmpty(){
        if(this.theStack.isEmpty())
            return true;
        return false;
    }

    public void push(BinaryTreeNode node){
        this.theStack.add(node);
    }

    public BinaryTreeNode pop(){
        if(!this.theStack.isEmpty())
            return this.theStack.remove(this.theStack.size() -1);
        return null;
    }


    // this is for debugging only
    public ArrayList<BinaryTreeNode> getStack(){
        return this.theStack;
    }

}
*/