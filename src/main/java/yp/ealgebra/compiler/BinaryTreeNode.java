/*
 * File         : BinaryTreeNode.java
 *
 * Date         : Thursday 23 January 2020
 *
 * Description  : This is the tree node, the most important file of the structure, and will be used for every leaf
 *
 * History      : 23/01/2020   --v1.00
 *
 * Author       : Matthew Dalzell   100227419
 */

package yp.ealgebra.compiler;


import java.util.Map;

public class BinaryTreeNode {
    private Object value;
    private BinaryTreeNode left;
    private BinaryTreeNode right;
    // want to make some way of putting useful brackets in
    public static Map<String, Number> priority = Map.<String, Number>of(
            "+", 0,
            "-", 0,
            "#", 1,
            "*", 1,
            "/", 2,
            "^", 2,
            "U-", 3
    );

    // default constructor for it being empty
    public BinaryTreeNode(){
        // this is a constructor
    }
	
	// making it from another node, this should be avoided but may occur
	public BinaryTreeNode(BinaryTreeNode inNode){
		this.value = inNode.getValue();
		if(inNode.getLeft() != null)
			this.left = inNode.getLeft();
		if(inNode.getRight() != null)
			this.right = inNode.getRight();
	}
		

    // this is another important constructor
    public BinaryTreeNode(BinaryTree input){
        BinaryTreeNode inputNode = input.getRoot();
        this.value = inputNode.getValue();
        this.right = inputNode.getRight();
        this.left = inputNode.getLeft();
    }

    // making a node when you just have the value for it
    public BinaryTreeNode(Object value){
        this.value = value;
    }

    public BinaryTreeNode(BinaryTreeNode left, Object operator, BinaryTreeNode right){
        this.left = left;
        this.value = operator;
        this.right = right;
    }


    // these are getters for various ideas of the node, giving extra information about the given thing in the name
    public boolean hasValue(){
        return this.value != null;
    }

    public boolean hasLeft(){
        return this.left != null;
    }

    public boolean hasRight(){
        return this.right != null;
    }

    public boolean hasChildren(){
        return this.left != null && this.right != null;
    }

    public boolean hasChild(){
        return this.left != null ^ this.right != null;
    }

    // getters
    public Object getValue(){
        return this.value;
    }

    public BinaryTreeNode getLeft(){
        return this.left;
    }

    public BinaryTreeNode getRight(){
        return this.right;
    }


    // setters
    public void setValue(Object o){
        this.value = o;
    }

    public void setLeft(BinaryTreeNode node){
        this.left= node;
    }

    public void setRight(BinaryTreeNode node){
        this.right = node;
    }


    // setting information about them, often used when building trees
    public void setLeft(BinaryTree tree){
        this.left = tree.getRoot();
    }

    public void setRight(BinaryTree tree){
        this.right = tree.getRoot();
    }

    // used when traversing the whole tree
    public String toString(){
        return this.value.toString();
    }
}
